﻿using UnityEngine;
using UnityEngine.InputSystem;
using World;
using World.Pickup;

/// <summary>
/// Interaction layer for input devices and the player perspective into the world.
/// </summary>
[RequireComponent(typeof(CharacterController))]
public sealed class PlayerController : MonoBehaviour, Controls.IPlayerActions
{
	private CharacterController characterController;

	private Controls controls;

	private Transform mainCameraTransform;

	[Header("Movement & Camera Settings")]
	[SerializeField]
	private float speed = 10f;

	[SerializeField]
	private float minCameraAngle = -90f;

	[SerializeField]
	private float maxCameraAngle = 90f;

	[Header("Interaction Settings")]
	[SerializeField, Tooltip("Distance at which a pickup can be detected.")]
	private float pickupDistance = 2f;

	private BaseInteractable focusedInteractableNow;

	private BaseInteractable focusedInteractableLast;

	private BasePickup heldPickup;

	private Vector2 moveDirection;

	private Vector2 lookDirection;

	private float xRot = 0f;

	public BasePickup HeldPickup => this.heldPickup;

	public Vector3 Velocity => this.characterController.velocity;

	private void Awake()
	{
		this.controls = new Controls();
		this.characterController = this.GetComponent<CharacterController>();
		this.mainCameraTransform = Camera.main.transform;

		this.controls.Player.SetCallbacks(this);
	}

	/// <summary>
	/// Drops the currently held <see cref="World.Pickup.BasePickup" /> if one exists, returning
	/// <c>true</c> if a <see cref="World.Pickup.BasePickup" /> was actually dropped, otherwise
	/// <c>false</c>.
	/// </summary>
	/// <returns>
	/// <c>true</c> if a <see cref="World.Pickup.BasePickup" /> was actually dropped, otherwise
	/// <c>false</c>.
	/// </returns>
	public bool Drop()
	{
		if (this.heldPickup != null)
		{
			this.heldPickup.OnDropped.Invoke(this);

			this.heldPickup = null;

			return true;
		}

		return false;
	}

	private void Update()
	{
		if (GameManager.Instance.IsPlaying)
		{
			Look();
			Move();
			CheckPickup();
		}
	}

	private void Move()
	{
		float x = moveDirection.x;
		float z = moveDirection.y;
		var characterTransform = characterController.transform;

		Vector3 move = ((characterTransform.right * x) + (characterTransform.forward * z));

		characterController.SimpleMove(move * speed);
	}

	private void Look()
	{
		float mouseX = lookDirection.x;
		float mouseY = lookDirection.y;

		xRot -= mouseY;
		xRot = Mathf.Clamp(xRot, minCameraAngle, maxCameraAngle);

		Camera.main.transform.localRotation = Quaternion.Euler(xRot, 0f, 0f);
		characterController.transform.Rotate(Vector3.up * mouseX);
	}

	private void CheckPickup()
	{
		var objectHit = new RaycastHit();

		Physics.Raycast(
			origin: this.mainCameraTransform.position,
			direction: this.mainCameraTransform.forward,
			hitInfo: out objectHit,
			maxDistance: this.pickupDistance,
			layerMask: Utility.Layers.Pickups.Mask
		);

		this.focusedInteractableLast = this.focusedInteractableNow;

		this.focusedInteractableNow = ((objectHit.collider == null) ?
				null : objectHit.collider.GetComponentInParent<BaseInteractable>());

		if (this.focusedInteractableNow == null)
		{
			Visual.OutlineEffect.Focus(null);
		}
		else if (this.focusedInteractableNow != this.focusedInteractableLast)
		{
			Visual.OutlineEffect.Focus(this.focusedInteractableNow.OutlineEffect);
		}
	}

	private void OnDestroy()
	{
		// Release non-GC resources.
		this.controls.Dispose();
	}

	private void OnDisable()
	{
		this.controls.Disable();
	}

	private void OnEnable()
	{
		//Awake();
		this.controls.Enable();
	}

	/// <summary>
	/// Input action callback triggered when the "drop" control is activated.
	/// </summary>
	/// <param name="context">Input action callback context.</param>
	public void OnDrop(InputAction.CallbackContext context)
	{
		// Can't drop something if the hand is empty.
		if (context.performed)
		{
			this.Drop();
		}
	}

	/// <summary>
	/// Input action callback triggered when the "look x" control is activated.
	/// </summary>
	/// <param name="context">Input action callback context.</param>
	public void OnLookX(InputAction.CallbackContext context)
	{
		this.lookDirection.x = context.ReadValue<float>();
	}

	/// <summary>
	/// Input action callback triggered when the "look y" control is activated.
	/// </summary>
	/// <param name="context">Input action callback context.</param>
	public void OnLookY(InputAction.CallbackContext context)
	{
		this.lookDirection.y = context.ReadValue<float>();
	}

	/// <summary>
	/// Input action callback triggered when the "move" control is activated.
	/// </summary>
	/// <param name="context">Input action callback context.</param>
	public void OnMove(InputAction.CallbackContext context)
	{
		this.moveDirection = context.ReadValue<Vector2>();
	}

	/// <summary>
	/// Input action callback triggered when the "pick up" control is activated.
	/// </summary>
	/// <param name="context">Input action callback context.</param>
	public void OnInteract(InputAction.CallbackContext context)
	{
		if (context.performed)
		{
			if (this.heldPickup == null)
			{
				if (this.focusedInteractableNow != null)
				{
					this.focusedInteractableNow.Interact(this, null);
				}
			}
			else
			{
				this.heldPickup.Interact(this, this.focusedInteractableNow);
			}
		}
	}

	/// <summary>
	/// Picks up the subject <see cref="World.Pickup.BasePickup" />.
	/// </summary>
	/// <param name="pickup">Subject <see cref="World.Pickup.BasePickup" /></param>
	public void PickUp(BasePickup pickup)
	{
		this.heldPickup = pickup;

		if (pickup != null)
		{
			Visual.OutlineEffect.Focus(null);
			pickup.OnPickedUp.Invoke(this);
		}
	}

	/// <summary>
	/// Input action callback triggered when the "pause" control is activated.
	/// </summary>
	/// <param name="context">Input action callback</param>
	public void OnPause(InputAction.CallbackContext context)
	{
		if(context.performed)
		{
			if (GameManager.Instance.IsPlaying)
			{
				GameManager.Instance.Pause();
			}
			else
			{
				GameManager.Instance.Resume();
			}
		}
	}

	/// <summary>
	/// Removes currently held pickup.
	/// </summary>
	public void RemovePickup()
	{
		if (this.heldPickup != null)
		{
			Destroy(this.heldPickup.gameObject);

			this.heldPickup = null;
		}
	}
}
