using World.Pickup;

namespace World
{
	/// <summary>
	/// Interaction behaviour for a beer cask that will pour beer when holding a
	/// <see cref="World.Pickup.DrinkPickup" />.
	/// </summary>
	public sealed class BeerCaskInteractable : BaseInteractable
	{
		[UnityEngine.SerializeField]
		private Data.DrinkType drinkType = null;

		/// <summary>
		/// Pours beer into a <see cref="World.Pickup.DrinkPickup" /> if one is held.
		///
		/// If the subject <see cref="World.Pickup.DrinkPickup" /> already contains a drink then
		/// nothing will happen.
		/// </summary>
		/// <param name="controller">Interacting controller.</param>
		/// <param name="focusedInteractable">
		/// <see cref="World.BaseInteractable" /> currently being held by the interacting
		/// controller.
		/// </param>
		public override void Interact(
				PlayerController controller, BaseInteractable focusedInteractable)
		{
			if (focusedInteractable is DrinkPickup drinkPickup)
			{
				drinkPickup.AddDrink(drinkType);
			}
		}
	}
}
