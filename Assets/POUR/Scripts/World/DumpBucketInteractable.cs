using World.Pickup;

namespace World
{
	/// <summary>
	/// Interaction behaviour for a bucket that can be used to dump the contents of
	/// <see cref="World.Pickup.DrinkPickup" />.
	/// </summary>
	public sealed class DumpBucketInteractable : BaseInteractable
	{
		/// <summary>
		/// Clears the contents of a <see cref="World.Pickup.DrinkPickup" />.
		/// </summary>
		/// <param name="controller">Interacting controller.</param>
		/// <param name="focusedInteractable">
		/// <see cref="World.BaseInteractable" /> currently being held by the interacting
		/// controller.
		/// </param>
		public override void Interact(
				PlayerController controller, BaseInteractable focusedInteractable)
		{
			if (focusedInteractable is DrinkPickup drinkPickup)
			{
				drinkPickup.ClearDrinks();
				drinkPickup.ClearModifiers();
			}
		}
	}
}
