using Data;
using UnityEngine;
using System.Collections.Generic;
using World.Pickup;
using Utility;

namespace World.Area
{
	/// <summary>
	/// Trigger region where a drink delivery may be made.
	/// </summary>
	[RequireComponent(typeof(Collider))]
	public sealed class DrinkDeliveryArea : MonoBehaviour
	{
		private const int maxDrinkOrder = 3;

		private const float orderDelayMin = 4.0f;

		private const float orderDelayMax = 10.0f;

		private const int scoreIncrement = 100;

		private Actor actor;

		[SerializeField]
		private DialogPanel requestDialogPanel = null;

		private DrinkRecipe[] requestedDrinkRecipes = null;

		private List<DrinkRecipe> pendingDrinkRecipes = new List<DrinkRecipe>();

		private Coroutine wait;

		private void Awake()
		{
			if (this.requestDialogPanel == null)
			{
				Debug.LogError("Missing RequestDialogPanel");
			}
		}

		private void OnTriggerEnter(Collider collider)
		{
			void SuccessState(DrinkRecipe[] requestedDrinks)
			{
				var characterHappySound = this.actor.Character.happySound;

				if (characterHappySound != null)
				{
					this.actor.AudioSource.PlayOneShot(characterHappySound);
				}

				this.actor.ParticleSystem.Play();

				foreach(DrinkRecipe recipe in requestedDrinkRecipes)
				{
					GameManager.Instance.Score += recipe.score;
				}

				this.requestedDrinkRecipes = null;

				this.pendingDrinkRecipes.Clear();
				this.requestDialogPanel.Hide();
				this.StopCoroutine(this.wait);

				this.wait = null;

				this.CallDeferred(1.0f, () => Destroy(this.actor.gameObject));
			}

			if (this.requestedDrinkRecipes != null)
			{
				var drinkPickup = collider.GetComponentInParent<DrinkPickup>();

				if (drinkPickup != null)
				{
					var character = this.actor.Character;

					Destroy(drinkPickup.gameObject);

					if (character.modifierFlags.HasFlag(CharacterModifierFlags.Underage))
					{
						// Underage order.
						if (!drinkPickup.HasDrink || drinkPickup.IsAlcoholic)
						{
							// Cannot serve them an empty or alcoholic drink.
							var angrySound = character.angrySound;
							GameManager.Instance.Score -= scoreIncrement;

							GameManager.Instance.DeductLives(1);

							if (angrySound != null)
							{
								this.actor.AudioSource.PlayOneShot(angrySound);
							}
						}
						else
						{
							// Correct.
							SuccessState(null);
						}
					}
					else
					{
						// Of-age order.
						var angrySound = character.angrySound;

                        for (var i = 0; i < pendingDrinkRecipes.Count; i += 1)
						{
                            if (drinkPickup.MatchesRecipe(this.pendingDrinkRecipes[i]))
							{
								// Correct.
								this.pendingDrinkRecipes.RemoveAt(i);

								if (this.pendingDrinkRecipes.Count == 0)
								{
									SuccessState(this.requestedDrinkRecipes);
								}
								else
								{
									this.UpdateSprite(this.pendingDrinkRecipes[0].iconSprite, Color.white);
								}

								return;
							}
						}

						// Incorrect.
						if (angrySound != null)
						{
							this.actor.AudioSource.PlayOneShot(angrySound);
						}

						GameManager.Instance.Score -= scoreIncrement;

						this.pendingDrinkRecipes.Clear();
						this.requestDialogPanel.Hide();
					}
				}
			}
		}

		public void SetActor(Actor newActor)
		{
			void DeregisterActor()
			{
				// TODO: Find a better solution than this.
				try {
					this.pendingDrinkRecipes.Clear();
					this.requestDialogPanel.Hide();

					this.requestedDrinkRecipes = null;
					this.actor = null;

					if (this.wait != null)
					{
						this.StopCoroutine(this.wait);

						this.wait = null;
					}
				} catch (System.Exception e) {}
			}

			if (this.actor != null)
			{
				DeregisterActor();
			}

			if (newActor != null)
			{
				this.actor = newActor;

				// The initial wait loop. This gets the characters preparing their orders.
				this.wait = this.CallDeferred(Random.Range(orderDelayMin, orderDelayMax), () =>
				{
					var preferredDrinkRecipes = newActor.Character.preferredDrinkRecipes;

					// Multiplied by two for higher quantity of drinks per order.
					this.requestedDrinkRecipes = Data.ObjectPool.GetRandomElements(
						preferredDrinkRecipes,
						Random.Range(1, Mathf.Min(preferredDrinkRecipes.Elements.Length, maxDrinkOrder))
					);

					this.pendingDrinkRecipes.AddRange(this.requestedDrinkRecipes);
					this.actor.OnDestroyed.AddListener(DeregisterActor);
					UpdateSprite(this.pendingDrinkRecipes[0].iconSprite, Color.white);

					// Second wait loop. This is the time they wait from placing their order til
					// leaving. Formula: 1 patience = 30 seconds of waiting.
					this.wait = this.CallDeferred((newActor.Character.patience * 30.0f), () =>
					{
						var characterAngrySound = this.actor.Character.angrySound;

						if (characterAngrySound != null)
						{
							this.actor.AudioSource.PlayOneShot(characterAngrySound);
						}

						var oldActor = this.actor;

						this.actor.Model.Hide();
						this.requestDialogPanel.Hide();
						this.SetActor(null);
						GameManager.Instance.Score -= 80;
						this.CallDeferred(1.5f, () => Destroy(oldActor.gameObject));
					});
				});
			}
		}

		private void UpdateSprite(Sprite sprite, Color color)
		{
			if(this.actor)
			{
				this.requestDialogPanel.ShowSprite(sprite, color);
				this.requestDialogPanel.transform.position =
					new Vector3(this.actor.transform.position.x,
								this.actor.transform.position.y + this.actor.DialogPanelYOffset,
								this.actor.transform.position.z);
			}
		}
	}
}
