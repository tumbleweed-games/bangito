using UnityEngine;
using Visual;

namespace World
{
	/// <summary>
	/// Abstract base for supporting interaction between the player and an object. Everything that
	/// responds to the "interact" control in-game uses <see cref="World.BaseInteractable" />.
	/// </summary>
	public abstract class BaseInteractable : MonoBehaviour
	{
		[SerializeField]
		private OutlineEffect outlineEffect;

		/// <summary>
		/// Associated <see cref="Visual.OutlineEffect" /> instance for displaying outlines around
		/// the <see cref="UnityEngine.GameObject" />.
		/// </summary>
		public OutlineEffect OutlineEffect => this.outlineEffect;

		/// <summary>
		/// Unity message that initialize any un-initialized state on the behalf of the
		/// <see cref="World.BaseInteractable" />.
		/// </summary>
		protected virtual void Awake()
		{
			// Outline effect may be supplied via serialized field.
			if (this.outlineEffect == null)
			{
				this.outlineEffect = this.GetComponent<OutlineEffect>();

				if (this.outlineEffect == null)
				{
					Debug.LogError("Unable to get Visual.OutlineEffect component");
				}
			}
		}

		public abstract void Interact(
				PlayerController controller, BaseInteractable focusedInteractable);
	}
}
