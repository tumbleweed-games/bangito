using Data;
using UnityEditor;
using UnityEngine;

namespace World.Spawning
{
	/// <summary>
	/// Spawner for prefabs containing a <see cref="World.Pickup.DrinkPickup" /> on the root game
	/// object.
	/// </summary>
	public sealed class DrinkPickupSpawner : BasePickupSpawner<Pickup.DrinkPickup>
	{
		[SerializeField]
		private DrinkGlass drinkGlass = null;

		/// <summary>
		/// Determines if <see cref="World.Spawning.DrinkPickupSpawner" /> is able to instantiate
		/// an instance, restricted based on whether or not a <see cref="Data.DrinkGlass" /> is
		/// currently assigned.
		/// </summary>
		protected override bool CanSpawn => (this.drinkGlass != null);

		/// <summary>
		/// Cooldown delay in seconds between the invocation of
		/// <see cref="World.Spawning.BaseSpawner.DeferredSpawn" /> and the spawning of a new
		/// <see cref="World.Pickup.DrinkPickup" /> instance.
		/// </summary>
		protected override float CooldownDuration => 1.0f;

		/// <summary>
		/// The currently assigned <see cref="Data.DrinkGlass" />.
		///
		/// This may be <c>null</c> when there isn't one assigned.
		/// </summary>
		public DrinkGlass DrinkGlass => this.drinkGlass;

		/// <summary>
		/// Attempts to assign a new <see cref="Data.DrinkGlass" />, returning <c>true</c> if it
		/// was successfully assigned, otherwise <c>false</c>.
		///
		/// Failure to assign is a result of
		/// <see cref="World.Spawning.BasePickupSpawner.PickupPrefab" /> being <c>null</c>.
		/// </summary>
		/// <param name="drinkGlass"><see cref="Data.DrinkGlass" /> to assign.</param>
		/// <returns><c>true</c> if successfully assigned, otherwise <c>false</c>.</returns>
		public bool AssignDrinkGlass(DrinkGlass drinkGlass)
		{
			if (this.PickupPrefab != null)
			{
				// Do not allow assignment if the pickup prefab has not yet been defined.
				this.drinkGlass = drinkGlass;

				return true;
			}

			this.drinkGlass = null;

			return false;
		}

		/// <summary>
		/// Assigns the current <see cref="Data.DrinkGlass" /> value to the spawned
		/// <see cref="World.Pickup.DrinkPickup" /> instance.
		/// </summary>
		protected override void OnSpawned()
		{
			this.OccupyingPickup.AssignDrinkGlass(this.drinkGlass);
		}
	}

#if UNITY_EDITOR
	/// <summary>
	/// Custom Unity inspector for <see cref="World.Spawning.DrinkPickupSpawner" />.
	/// </summary>
	[CustomEditor(inspectedType: typeof(DrinkPickupSpawner))]
	public sealed class DrinkPickupSpawnerEditor : Editor
	{
		private DrinkPickupSpawner spawner = null;

		/// <summary>
		/// Executes every time the GUI updates.
		/// </summary>
		public override void OnInspectorGUI()
		{
			var oldPickupPrefab = this.spawner.PickupPrefab;

			var pickupPrefab = (GameObject)EditorGUILayout.ObjectField(
				label: "Pickup Prefab",
				obj: oldPickupPrefab,
				objType: typeof(GameObject),
				allowSceneObjects: false
			);

			if (pickupPrefab != oldPickupPrefab)
			{
				if (!this.spawner.AssignPickupPrefab(pickupPrefab))
				{
					// Should the assignment fail due to an invalid game object, it is reasonable
					// to reset its item to null in case anything else erroneously tries to
					// reference it.
					this.spawner.AssignDrinkGlass(null);
				}

				EditorUtility.SetDirty(this.spawner);
			}

			var pickup = DrinkPickupSpawner.GetComponentOf(this.spawner.PickupPrefab);

			if (pickup != null)
			{
				var oldItem = this.spawner.DrinkGlass;

				var item = (DrinkGlass)EditorGUILayout.ObjectField(
					label: "Drink Item",
					obj: oldItem,
					objType: typeof(DrinkGlass),
					allowSceneObjects: false
				);

				if (item != oldItem)
				{
					this.spawner.AssignDrinkGlass(item);
					EditorUtility.SetDirty(this.spawner);
				}
			}
		}

		private void Awake()
		{
			this.spawner = (DrinkPickupSpawner)this.target;
		}
	}
#endif
}
