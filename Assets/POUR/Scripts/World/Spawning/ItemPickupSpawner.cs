using Data.Item;
using UnityEditor;
using UnityEngine;

namespace World.Spawning
{
	/// <summary>
	/// Spawner for <see cref="World.Pickup.BaseItemPickup" />-derived instances that represent
	/// <see cref="Data.Item.BaseItem" />-derived types.
	/// </summary>
	public sealed class ItemPickupSpawner : BasePickupSpawner<Pickup.BaseItemPickup>
	{
		[SerializeField]
		private float spawnCooldown = 10.0f;

		[SerializeField]
		private BaseItem item = null;

		/// <summary>
		/// Determines if <see cref="World.Spawning.ItemPickupSpawner" /> is able to instantiate
		/// an instance, restricted based on whether or not a <see cref="Data.Item.BaseItem" /> is
		/// currently assigned.
		/// </summary>
		protected override bool CanSpawn => (this.item != null);

		/// <summary>
		/// Cooldown delay in seconds between the invocation of
		/// <see cref="World.Spawning.BaseSpawner.DeferredSpawn" /> and the spawning of a new
		/// <see cref="World.Pickup.DrinkPickup" /> instance.
		/// </summary>
		protected override float CooldownDuration => this.spawnCooldown;

		/// <summary>
		/// Item for the spawned <see cref="World.Pickup.BaseItemPickup" /> to hold.
		/// </summary>
		public BaseItem Item => this.item;

		/// <summary>
		/// Attempts to assign a new <see cref="Data.Item.BaseItem" />, returning <c>true</c> if it
		/// was successfully assigned, otherwise <c>false</c>.
		///
		/// Failure to assign is a result of
		/// <see cref="World.Spawning.BasePickupSpawner.PickupPrefab" /> being <c>null</c>.
		/// </summary>
		/// <param name="item"><see cref="Data.Item.BaseItem" /> to assign.</param>
		/// <returns><c>true</c> if successfully assigned, otherwise <c>false</c>.</returns>
		public bool AssignItem(BaseItem item)
		{
			if (this.PickupPrefab != null)
			{
				// Do not allow assignment if the pickup prefab has not yet been defined.
				this.item = item;

				return true;
			}

			this.item = null;

			return false;
		}

		/// <summary>
		/// Assigns the current <see cref="Data.Item.BaseItem" /> value to the spawned
		/// <see cref="World.Pickup.BaseItemPickup" /> instance.
		/// </summary>
		protected override void OnSpawned()
		{
			this.OccupyingPickup.AssignItem(item);
		}
	}

#if UNITY_EDITOR
	/// <summary>
	/// Custom Unity inspector for <see cref="World.Spawning.ItemPickupSpawner" />
	/// </summary>
	[CustomEditor(inspectedType: typeof(ItemPickupSpawner))]
	public sealed class ItemPickupSpawnerEditor : Editor
	{
		private ItemPickupSpawner spawner = null;

		/// <summary>
		/// Executes every time the GUI updates.
		/// </summary>
		public override void OnInspectorGUI()
		{
			var oldPickupPrefab = this.spawner.PickupPrefab;

			var pickupPrefab = (GameObject)EditorGUILayout.ObjectField(
				label: "Pickup Prefab",
				obj: oldPickupPrefab,
				objType: typeof(GameObject),
				allowSceneObjects: false
			);

			if (pickupPrefab != oldPickupPrefab)
			{
				if (!this.spawner.AssignPickupPrefab(pickupPrefab))
				{
					// Should the assignment fail due to an invalid game object, it is reasonable
					// to reset its item to null in case anything else erroneously tries to
					// reference it.
					this.spawner.AssignItem(null);
				}

				EditorUtility.SetDirty(this.spawner);
			}

			var pickup = ItemPickupSpawner.GetComponentOf(this.spawner.PickupPrefab);

			if (pickup != null)
			{
				var itemType = pickup.GetItemType();
				var oldItem = this.spawner.Item;

				// Dynamic cast as a sanity check. While it's pretty safe to assume that "itemType"
				// is a "BaseItem"-derived type there are no in-language restrictions for it.
				var item = (EditorGUILayout.ObjectField(
					label: itemType.Name,
					obj: oldItem,
					objType: itemType,
					allowSceneObjects: false
				) as BaseItem);

				if (item != oldItem)
				{
					this.spawner.AssignItem(item);
					EditorUtility.SetDirty(this.spawner);
				}
			}

			EditorGUILayout.PropertyField(this.serializedObject.FindProperty("spawnCooldown"));
			this.serializedObject.ApplyModifiedProperties();
		}

		private void Awake()
		{
			this.spawner = (ItemPickupSpawner)this.target;
		}
	}
#endif
}
