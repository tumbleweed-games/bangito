using UnityEngine;
using World.Pickup;

namespace World.Spawning
{
	public abstract class BasePickupSpawner : BaseSpawner
	{
		private BasePickup occupyingPickup = null;

		protected abstract bool CanSpawn
		{
			get;
		}

		/// <summary>
		/// Reference to the <see cref="World.Pickup.BasePickup" /> of the game object currently
		/// occupying the <see cref="World.Spawning.BasePickupSpawner" />.
		///
		/// This may be <c>null</c> if nothing is occupying the spawner.
		/// </summary>
		public BasePickup OccupyingPickup
		{
			get => this.occupyingPickup;
			protected set => this.occupyingPickup = value;
		}

		public abstract bool AssignPickupPrefab(GameObject gameObject);
	}

	/// <summary>
	/// Base spawning behaviour for <see cref="World.Pickup.BasePickup" />-derived types.
	///
	/// To implement this as a concrete <see cref="UnityEngine.MonoBehaviour" /> simply extend it,
	/// passing the <see cref="World.Pickup.BasePickup" />-derived type as the generic parameter
	/// and provide satisfactory implementations for the abstract methods and properties.
	/// </summary>
	/// <typeparam name="T">Pickup type to allow assigned for spawning instances of.</typeparam>
	public abstract class BasePickupSpawner<T> : BasePickupSpawner where T : BasePickup
	{
		[SerializeField]
		private GameObject pickupPrefab = null;

		/// <summary>
		/// Currently occupying instance referenced via its <see cref="World.Pickup.BasePickup" />-
		/// derived component type <c>T</c>.
		///
		/// This property is not particularly useful outside of run-time as it will always be
		/// <c>null</c> until a run-time event or behaviour invokes either
		/// <see cref="World.Spawning.BaseSpawner.DeferredSpawn" /> or
		/// <see cref="World.Spawning.BaseSpawner.ImmediateSpawn" />.
		/// </summary>
		public new T OccupyingPickup
		{
			get => (base.OccupyingPickup as T);
			set => base.OccupyingPickup = value;
		}

		/// <summary>
		/// Prefab <see cref="UnityEngine.GameObject" /> instnatiated whenever
		/// <see cref="World.Spawning.BaseSpawner.DeferredSpawn" /> or
		/// <see cref="World.Spawning.BaseSpawner.ImmediateSpawn" /> are invoked.
		///
		/// This property defaults to <c>null</c> in the unassigned state.
		/// </summary>
		public GameObject PickupPrefab => this.pickupPrefab;

		/// <summary>
		/// Attempts to assign the <see cref="UnityEngine.GameObject" />, returning <c>true</c> if
		/// it can be assigned to the <see cref="World.Spawning.BasePickupSpawner{T}" />, otherwise
		/// <c>false</c>.
		///
		/// Validity of whether or not a <see cref="UnityEngine.GameObject" /> can be assigned as
		/// the prefab is determined by wether or not it contains a component of type <c>T</c> on
		/// the root game object.
		/// </summary>
		/// <param name="gameObject">Pickup prefab candidate.</param>
		/// <returns>
		/// <c>true</c> if it can be assigned to the
		/// <see cref="World.Spawning.BasePickupSpawner{T}" />, otherwise <c>false</c>.
		/// </returns>
		public sealed override bool AssignPickupPrefab(GameObject gameObject)
		{
			if (GetComponentOf(gameObject) != null)
			{
				// The pickup game object is a valid, non-null pickup game object.
				this.pickupPrefab = gameObject;

				return true;
			}

			this.pickupPrefab = null;

			Debug.LogWarning("GameObject must contain a " +
					typeof(T).FullName + " in order to be assigned to this spawner");

			return false;
		}

		protected abstract void OnSpawned();

		/// <summary>
		/// Spawns an instance of <see cref="World.Pickup.BasePickup" />.
		/// </summary>
		protected sealed override void Spawn()
		{
			var occupyingPickup = this.OccupyingPickup;

			if (occupyingPickup != null)
			{
				// Clean up anything that still exists, effectively overriding the actor present
				// here.
				Destroy(occupyingPickup);

				// This may also be set to null by the "Actor.OnDestroy" event, but it is done here
				// as a sanity check as the time at which the events may fire off probably isn't
				// inside the lifetime of this function considering everything runs synchronously.
				this.OccupyingPickup = null;
			}

			if ((this.pickupPrefab != null) && this.CanSpawn)
			{
				var gameObject = Instantiate(
					original: this.pickupPrefab,
					position: this.transform.position,
					rotation: Quaternion.identity
				);

				if (gameObject == null)
				{
					Debug.LogError("Failed to spawn pickup due to allocation failure");
				}
				else
				{
					var pickup = gameObject.GetComponent<T>();

					if (pickup == null)
					{
						this.OccupyingPickup = null;

						Debug.LogError("Attempt to spawn game object that does not have a " +
								typeof(T).FullName);
					}
					else
					{
						this.OccupyingPickup = pickup;

						// Makes sure the reference is reset once the item is gone.
						pickup.OnDestroyed.AddListener(() =>
						{
							this.OccupyingPickup = null;

							this.DeferredSpawn();
						});

						this.OnSpawned();
					}
				}
			}
		}

		/// <summary>
		/// Utility for returning the component of type <C>T</C> from the
		/// <see cref="UnityEngine.GameObject" /> in a <c>null</c>-safe manner, returning
		/// <c>null</c> if either it or the subject <see cref="World.Pickup.BasePickup" /> are
		/// <c>null</c>.
		/// </summary>
		/// <param name="gameObject">
		/// Subject <see cref="UnityEngine.GameObject" /> to derive the type <c>T</c> from.
		/// </param>
		/// <returns>
		/// Component of type <C>T</C> from the <see cref="UnityEngine.GameObject" />, returning
		/// <c>null</c> if either it or the subject <see cref="World.Pickup.BasePickup" /> are
		/// <c>null</c>.
		/// </returns>
		public static T GetComponentOf(GameObject gameObject)
		{
			return ((gameObject == null) ? null : gameObject.GetComponent<T>());
		}

		/// <summary>
		/// Unity callback used to signal when the owning <see cref="UnityEngine.GameObject" /> of
		/// the subject <see cref="UnityEngine.Component" /> is destroyed.
		/// </summary>
		protected virtual void OnDestroy()
		{
			var occupyingPickup = this.OccupyingPickup;

			if (occupyingPickup != null)
			{
				// No point resetting "occupyingPickup" to null as its memory will be destroyed
				// momentarily as well.
				Destroy(occupyingPickup);
			}
		}

#if UNITY_EDITOR
		/// <summary>
		/// Validates that the occupying pickup component is of the same type as <c>T</c>.
		/// </summary>
		protected virtual void OnValidate()
		{
			var occupyingPickup = this.OccupyingPickup;

			if ((occupyingPickup != null) && (GetComponentOf(this.pickupPrefab) == null))
			{
				occupyingPickup = null;
			}
		}
#endif
	}
}
