using UnityEngine;
using Utility;

namespace World.Spawning
{
	/// <summary>
	/// Abstract base for handling game-intrinsic details for spawning
	/// <see cref="UnityEngine.GameObject" />s, such as the delay between deferred spawns and the
	/// interface for spawning instances.
	/// </summary>
	public abstract class BaseSpawner : MonoBehaviour
	{
		private Coroutine deferredSpawn;

		protected abstract float CooldownDuration
		{
			get;
		}

		/// <summary>
		/// Spawns a <see cref="UnityEngine.GameObject" /> instance defined by an inheriting
		/// <c>class</c> after the duration of time defined by
		/// <see cref="World.Spawning.BaseSpawner.CooldownDuration" /> has elapsed.
		/// </summary>
		public void DeferredSpawn()
		{
			if (this.deferredSpawn != null)
			{
				this.StopCoroutine(this.deferredSpawn);

				this.deferredSpawn = null;
			}

			this.deferredSpawn = this.CallDeferred(this.CooldownDuration, this.Spawn);
		}

		/// <summary>
		/// Spawns a <see cref="UnityEngine.GameObject" /> instance defined by an inheriting
		/// <c>class</c> immediately, cancelling any deferred spawn queued by
		/// <see cref="World.Spawning.BaseSpawner.DeferredSpawn" /> that had not yet executed.
		/// </summary>
		public void ImmediateSpawn()
		{
			if (this.deferredSpawn != null)
			{
				this.StopCoroutine(this.deferredSpawn);

				this.deferredSpawn = null;
			}

			this.Spawn();
		}

		protected abstract void Spawn();
	}
}
