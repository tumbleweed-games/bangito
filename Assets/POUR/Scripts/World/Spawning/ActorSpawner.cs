using Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Timeline;

namespace World.Spawning
{
	public sealed class ActorSpawner : BaseSpawner
	{
		private const float cooldownMin = 5.0f;

		private const float cooldownMax = 12.0f;

		[SerializeField]
		private GameObject actorPrefab = null;

		[SerializeField]
		private CharacterPool characterPool = null;

		[SerializeField]
		private World.Area.DrinkDeliveryArea drinkDeliveryArea = null;

		[SerializeField]
		private UnityEvent onSpawned = new UnityEvent();

		private Actor occupier;

		public UnityEvent OnSpawned => this.onSpawned;

		protected override float CooldownDuration => Random.Range(cooldownMin, cooldownMax);

		private Vector3 markerPosition;

		private void Start()
		{
			var marker = GameObject.Find("Marker");
			markerPosition = marker ? marker.transform.position : Vector3.zero;
		}

		protected override void Spawn()
		{
			if (this.occupier != null)
			{
				// Clean up anything that still exists, effectively overriding the actor present
				// here.
				Destroy(this.occupier.gameObject);

				this.occupier = null;
			}

			if (this.characterPool != null)
			{
				var actorInstance = GameObject.Instantiate(this.actorPrefab);

				if (actorInstance != null)
				{
					this.occupier = actorInstance.GetComponent<Actor>();

					if (this.occupier != null)
					{
						var actorTransform = this.occupier.transform;
						var playerObject = GameObject.Find("Marker");

						this.occupier.AssignCharacter(
								ObjectPool.GetRandomElement(this.characterPool));

						if (playerObject != null)
						{
							actorTransform.LookAt(playerObject.transform.position);
						}

						actorTransform.position = this.transform.position;

						if (this.drinkDeliveryArea != null)
						{
							this.drinkDeliveryArea.SetActor(this.occupier);
						}

						this.occupier.OnDestroyed.AddListener(this.DeferredSpawn);
					}
					else
					{
						Debug.LogError("Failed to acquire Actor component");
					}
				}
				else
				{
					Debug.LogError("Failed to spawn Actor due to allocation failure or none");
				}
			}
		}

		private void Update()
		{
			if (markerPosition != null && this.occupier)
			{
				this.occupier.transform.LookAt(markerPosition);
			}
		}
	}
}
