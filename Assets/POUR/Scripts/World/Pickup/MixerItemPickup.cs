using Data.Item;
using System;

namespace World.Pickup
{
	/// <summary>
	/// Worldspace item representation for a <see cref="Data.Item.MixerItem" />.
	/// </summary>
	public class MixerItemPickup : BaseItemPickup
	{
		private int remainingUses;

		/// <summary>
		/// Currently held <see cref="Data.Item.MixerItem" />.
		/// </summary>
		public new MixerItem Item => (base.Item as MixerItem);

		/// <summary>
		/// Returns type information for the <see cref="Data.Item.BaseItem" />-derived type held
		/// within.
		/// </summary>
		/// <returns>
		/// Type information for the <see cref="Data.Item.BaseItem" />-derived type held within.
		/// </returns>
		public override Type GetItemType()
		{
			return typeof(MixerItem);
		}

		/// <summary>
		/// Resets the remaining uses of the <see cref="World.Pickup.MixerItemPickup" /> to
		/// whatever the new remaining uses of the new <see cref="Data.Item.MixerItem" /> reference
		/// data is.
		/// </summary>
		/// <param name="oldItem">Previous <see cref="World.Pickup.MixerItemPickup" />.</param>
		protected override void OnItemChange(BaseItem oldItem)
		{
			this.remainingUses = this.Item.uses;
		}

		/// <summary>
		/// Applies the <see cref="World.Pickup.MixerItemPickup" />
		/// <see cref="Data.Item.MixerItem" /> information to the focused
		/// <see cref="World.Pickup.BasePickup" /> if applicable, otherwise doing nothing.
		/// </summary>
		/// <param name="controller">Subject controller.</param>
		/// <param name="focusedPickup">Focused <see cref="World.Pickup.BasePickup" />.</param>
		protected override void Use(
				PlayerController controller, BaseInteractable focusedInteractable)
		{
			if (focusedInteractable is DrinkPickup drinkPickup)
			{
				var itemModifierFlags = this.Item.modifierFlags;

				if (drinkPickup.EnableModifiers(itemModifierFlags).HasFlag(itemModifierFlags))
				{
					// Check that all modifier flags have been successfully set (indicating that
					// the target drink pickup has all of the applicable components for the
					// mixers).
					if (this.remainingUses != 0)
					{
						// A zero value is treated as an infinite resource, so first check it's not
						// already zero so the mixer resource may be depleted by one and, if then
						// zero, destroyed.
						this.remainingUses -= 1;

						if (this.remainingUses == 0)
						{
							controller.RemovePickup();
						}
					}
				}
			}
		}
	}
}
