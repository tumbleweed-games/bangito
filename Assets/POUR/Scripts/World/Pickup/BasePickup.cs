﻿using UnityEngine;
using UnityEngine.Events;

namespace World.Pickup
{
	/// <summary>
	/// Represents a physical <see cref="Data.Item" /> instance.
	/// </summary>
	[RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(AudioSource))]
	public abstract class BasePickup : BaseInteractable
	{
		/// <summary>
		/// Callback for handling items that have fallen from an arbitrary distance.
		/// </summary>
		public class LandedEvent : UnityEvent<float> {}

		public class InteractedEvent : UnityEvent<PlayerController> {}

		[SerializeField]
		private InteractedEvent onPickedUp = new InteractedEvent();

		[SerializeField]
		private UnityEvent onDestroyed = new UnityEvent();

		[SerializeField]
		private InteractedEvent onDropped = new InteractedEvent();

		[SerializeField]
		private LandedEvent onLanded = new LandedEvent();

		private Transform priorParent;

		private float priorHeight;

		[SerializeField]
		private new Rigidbody rigidbody;

		[SerializeField]
		private AudioSource audioSource;

		/// <summary>
		/// <see cref="UnityEngine.Collider" /> associated with pick-up interaction.
		/// </summary>
		protected abstract Collider Collider
		{
			get;
		}

		/// <summary>
		/// Euler angles to offset the held transform by.
		/// </summary>
		protected abstract Vector3 OffsetEulerAngle
		{
			get;
		}

		/// <summary>
		/// Position to offset the held transform by.
		/// </summary>
		protected abstract Vector3 OffsetPosition
		{
			get;
		}

		/// <summary>
		/// Events that execute once a <see cref="World.Pickup.BaseItemPickup" /> has been
		/// destroyed.
		/// </summary>
		public UnityEvent OnDestroyed => this.onDestroyed;

		/// <summary>
		/// Events that execute once a <see cref="World.Pickup.BasePickup" /> has been dropped.
		/// </summary>
		public InteractedEvent OnDropped => this.onDropped;

		/// <summary>
		/// Events that execute once a <see cref="World.Pickup.BasePickup" /> has landed after
		/// falling.
		/// </summary>
		public LandedEvent OnLanded => this.onLanded;

		/// <summary>
		/// Events that execute once a <see cref="World.Pickup.BasePickup" /> has been picked up.
		/// </summary>
		public InteractedEvent OnPickedUp => this.onPickedUp;

		/// <summary>
		/// Unity message that triggers when the <see cref="UnityEngine.GameObject" /> is created.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			// Rigidbody may be supplied via serialized field.
			if (this.rigidbody == null)
			{
				this.rigidbody = this.GetComponent<Rigidbody>();

				if (this.rigidbody == null)
				{
					Debug.LogError("Unable to get UnityEngine.Rigidbody component");
				}
			}

			// Audio source may be supplied via serialized field.
			if (this.audioSource == null)
			{
				this.audioSource = this.GetComponent<AudioSource>();

				if (this.audioSource == null)
				{
					Debug.LogError("Unable to get UnityEngine.AudioSource component");
				}
			}

			this.onPickedUp.AddListener((controller) =>
			{
				// Save current transform data to restore from later.
				this.priorParent = this.transform.parent;
				// Update the transform to follow the player.
				this.transform.parent = controller.transform;
				this.transform.localPosition = (Vector3.forward + this.OffsetPosition);
				this.transform.localEulerAngles = this.OffsetEulerAngle;
				// Update the rigidbody and collider properties.
				this.rigidbody.useGravity = false;
				this.rigidbody.isKinematic = true;
				this.Collider.enabled = false;
				this.gameObject.layer = Utility.Layers.Default.Ordinal;
			});

			this.onDropped.AddListener((controller) =>
			{
				// The held height is now the previous height.
				this.priorHeight = this.transform.position.y;
				// Restore from the transform state.
				this.transform.parent = this.priorParent;
				// Update the rigidbody and collider properties.
				this.rigidbody.useGravity = true;
				this.rigidbody.isKinematic = false;
				this.Collider.enabled = true;
				this.gameObject.layer = Utility.Layers.Pickups.Ordinal;
				this.rigidbody.velocity = (controller.Velocity / 3);
			});
		}

		public override void Interact(
				PlayerController controller, BaseInteractable focusedInteractable)
		{
			if (controller.HeldPickup == null)
			{
				// Pick up the item as usual.
				controller.PickUp(this);
			}
			else
			{
				// It's already being held, so use it.
				this.Use(controller, focusedInteractable);
			}
		}

		/// <summary>
		/// Message triggered when a collision event occurs with the attached
		/// <see cref="UnityEngine.GameObject" />.
		/// </summary>
		/// <param name="collision">Detected collision.</param>
		protected virtual void OnCollisionEnter(Collision collision)
		{
			var fallDistance =  (this.priorHeight - this.transform.position.y);

			if (fallDistance > 0.0f)
			{
				this.onLanded.Invoke(fallDistance);
			}
		}

		/// <summary>
		/// Message triggered when a collision event ends with the attached
		/// <see cref="UnityEngine.GameObject" />.
		/// </summary>
		/// <param name="collision">Detected collision.</param>
		protected virtual void OnCollisionExit(Collision collision)
		{
			this.priorHeight = this.transform.position.y;
		}

		/// <summary>
		/// Unity callback used to signal when the owning <see cref="UnityEngine.GameObject" /> of
		/// the subject <see cref="UnityEngine.Component" /> is destroyed.
		/// </summary>
		private void OnDestroy()
		{
			this.onDestroyed.Invoke();
		}

		/// <summary>
		/// Plays a <see cref="UnityEngine.AudioClip" />.
		/// </summary>
		/// <param name="audioClip"><see cref="UnityEngine.AudioClip" /> to play.</param>
		public void PlayAudio(AudioClip audioClip)
		{
			this.audioSource.PlayOneShot(audioClip);
		}

		protected virtual void Use(
				PlayerController controller, BaseInteractable focusedInteractable)
		{
			if ((focusedInteractable != null) && !(focusedInteractable is BasePickup))
			{
				// Switcharoo this to the caller to see if it has any implemented behaviour. The
				// reason there's type check is so that it doesn't waste time calling back to try
				// and interact with another pick-up, which would not only be pointless but also
				// potentially very fatal as it could blow out the call stack.
				focusedInteractable.Interact(controller, this);
			}
		}
	}
}
