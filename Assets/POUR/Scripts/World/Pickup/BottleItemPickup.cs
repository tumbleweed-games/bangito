using Data.Item;
using Model.Item;
using System;
using UnityEngine;

namespace World.Pickup
{
	/// <summary>
	/// Worldspace item representation for a <see cref="Data.Item.BottleItem" />.
	/// </summary>
	[RequireComponent(typeof(Animator))]
	public class BottleItemPickup : BaseItemPickup
	{
		private Animator bottleAnimator;

		/// <summary>
		/// Currently held <see cref="Data.Item.BottleItem" />.
		/// </summary>
		public new BottleItem Item => (base.Item as BottleItem);

		/// <summary>
		/// <see cref="Model.Item.BottleItemModel" /> instance.
		/// </summary>
		public new BottleItemModel Model => (base.Model as BottleItemModel);

		/// <summary>
		/// Prepares up the <see cref="World.Pickup.BottleItemPickup" /> as soon as the containing
		/// game object is created.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			this.bottleAnimator = this.GetComponent<Animator>();

			if (this.bottleAnimator == null)
			{
				Debug.LogError("Unable to get UnityEngine.Animator component");
			}

			// For playing the cork pop effect whenever the item is picked up.
			this.OnPickedUp.AddListener((controller) =>
			{
				var corkEffect = this.Model.CorkPopEffect;

				if (corkEffect != null)
				{
					corkEffect.Enable();
				}
			});

			// For disabling the cork pop effect whenever the item is dropped.
			this.OnDropped.AddListener((controller) =>
			{
				var corkEffect = this.Model.CorkPopEffect;

				if (corkEffect != null)
				{
					corkEffect.Disable();
				}
			});
		}

		/// <summary>
		/// Callback used by the animator component to begin pouring.
		/// </summary>
		public void OnEnterPour()
		{
			var liquidEffect = this.Model.LiquidEffect;
			var item = this.Item;

			if (liquidEffect != null)
			{
				liquidEffect.SetFill(item.pouringFillValue);
			}

			if (item.pouringSound != null)
			{
				this.PlayAudio(item.pouringSound);
			}
		}

		/// <summary>
		/// Callback used by the animator component to halt pouring.
		/// </summary>
		public void OnExitPour()
		{
			var liquidEffect = this.Model.LiquidEffect;

			if (liquidEffect != null)
			{
				liquidEffect.SetFill(1.0f);
			}
		}

		/// <summary>
		/// Returns type information for the <see cref="Data.Item.BaseItem" />-derived type held
		/// within.
		/// </summary>
		/// <returns>
		/// Type information for the <see cref="Data.Item.BaseItem" />-derived type held within.
		/// </returns>
		public override Type GetItemType()
		{
			return typeof(BottleItem);
		}

		/// <summary>
		/// Updates the color of the <see cref="Data.Item.BottleItem" /> model liquid, if it has
		/// one.
		/// </summary>
		/// <param name="oldItem">Previous <see cref="World.Pickup.BottleItem" />.</param>
		protected override void OnItemChange(BaseItem oldItem)
		{
			var item = this.Item;

			if (item != null)
			{
				var liquidEffect = this.Model.LiquidEffect;

				if (liquidEffect != null)
				{
					liquidEffect.SetColor(item.drinkType.liquidColor);
				}
			}
		}

		/// <summary>
		/// Toggles pouring the <see cref="World.Pickup.BottleItemPickup" />.
		/// </summary>
		protected override void Use(
				PlayerController controller, BaseInteractable focusedInteractable)
		{
			if (focusedInteractable is DrinkPickup drinkPickup)
			{
				drinkPickup.AddDrink(this.Item.drinkType);
				this.bottleAnimator.SetTrigger("Use");
			}
		}
	}
}
