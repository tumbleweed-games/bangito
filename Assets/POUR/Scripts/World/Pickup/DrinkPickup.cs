using Data;
using System.Collections;
using System.Collections.Generic;
using Model;
using UnityEditor;
using UnityEngine;
using Visual;

namespace World.Pickup
{
	/// <summary>
	/// Represents a drink, analagous to the <see cref="Data.DrinkRecipe" /> type in terms of
	/// contained data.
	/// </summary>
	public sealed class DrinkPickup : BasePickup
	{
		private static readonly AnimationCurve liquidAnimationCurve =
				AnimationCurve.Linear(0, 0, 1, 1);

		[SerializeField]
		private GlassModel model;

		[SerializeField]
		private DrinkGlass drinkGlass;

		private HashSet<DrinkType> drinkTypes = new HashSet<DrinkType>();

		private DrinkModifierFlags modifierFlags;

		private Material restoreMaterial;

		/// <summary>
		/// Reference to the <see cref="UnityEngine.Collider" /> responsible for pickup
		/// interaction.
		/// </summary>
		protected override Collider Collider => this.model.Collider;

		/// <summary>
		/// <see cref="Data.DrinkGlass" /> instance.
		/// </summary>
		public DrinkGlass DrinkGlass => this.drinkGlass;

		public bool HasDrink => (this.drinkTypes.Count != 0);

		/// <summary>
		/// Physical representation in the world.
		/// </summary>
		public GlassModel Model => this.model;

		public bool IsAlcoholic
		{
			get
			{
				foreach (var drinkType in this.drinkTypes)
				{
					if (drinkType.ethanolVolume > 0.0f)
					{
						return true;
					}
				}

				return false;
			}
		}

		/// <summary>
		/// Euler angles to offset the held transform by.
		/// </summary>
		protected override Vector3 OffsetEulerAngle => Vector3.zero;

		/// <summary>
		/// Position to offset the held transform by.
		/// </summary>
		protected override Vector3 OffsetPosition => Vector3.zero;

		/// <summary>
		/// Adds a new <see cref="Data.DrinkType" /> to the <see cref="World.Pickup.DrinkPickup" />
		/// if it isn't already.
		///
		/// If the <see cref="World.Pickup.DrinkPickup" /> already contains another
		/// <see cref="Data.DrinkType" /> the <see cref="World.Pickup.DrinkPickup" /> becomes a
		/// mixture of the two.
		/// </summary>
		/// <param name="drinkType">New <see cref="Data.DrinkType" />.</param>
		public void AddDrink(DrinkType drinkType)
		{
			IEnumerator FillDrink(BottledLiquidEffect effect)
			{
				const float duration = 2.0f;
				var startFillValue = effect.FillValue;
				var time = 0.0f;

				while (effect.FillValue < BottledLiquidEffect.fillValueMax)
				{
					time += Time.deltaTime;

					effect.SetFill(Mathf.Lerp(
						startFillValue,
						BottledLiquidEffect.fillValueMax,
						liquidAnimationCurve.Evaluate(time / duration)
					));

					yield return Utility.Wait.ForEndOfFrame;
				}
			}

			if (this.drinkTypes.Add(drinkType))
			{
				var effect = this.model.LiquidEffect;

				if (effect != null)
				{
					effect.SetColor((drinkType.liquidColor + effect.Color) * 0.5f);
					this.StartCoroutine(FillDrink(effect));
				}
			}
		}

		/// <summary>
		/// Assigns a new <see cref="Data.DrinkGlass" /> instance, replacing the previous one and
		/// updating the <see cref="Model.GlassModel" />.
		/// </summary>
		/// <param name="newDrinkGlass">New instance.</param>
		public void AssignDrinkGlass(DrinkGlass newDrinkGlass)
		{
			BaseModel.DestroyModel(this.model);

			if (newDrinkGlass == null)
			{
				this.model = null;

				// Disable while no item is assigned.
				this.gameObject.SetActive(false);
			}
			else
			{
				this.model = BaseModel.
						InstantiateModel<GlassModel>(newDrinkGlass.modelPrefab, this.transform);

				if (this.model != null)
				{
					var modelTransform = this.model.transform;
					modelTransform.position = this.transform.position;
					modelTransform.parent = this.transform;
				}

				// Re-enable if disabled.
				this.gameObject.SetActive(true);
			}

			this.drinkGlass = newDrinkGlass;

			// Re-scan renderers for the contained model.
			this.OutlineEffect.UpdateRenderers();
		}

		/// <summary>
		/// Clears all currently assigned <see cref="Data.DrinkType" />s.
		/// </summary>
		public void ClearDrinks()
		{
			IEnumerator EmptyDrink(BottledLiquidEffect effect)
			{
				const float duration = 2.0f;
				var startFillValue = effect.FillValue;
				var time = 0.0f;

				while (effect.FillValue > BottledLiquidEffect.fillValueMin)
				{
					time += Time.deltaTime;

					effect.SetFill(Mathf.Lerp(
						startFillValue,
						BottledLiquidEffect.fillValueMin,
						liquidAnimationCurve.Evaluate(time / duration)
					));

					yield return Utility.Wait.ForEndOfFrame;
				}
			}

			var liquidEffect = this.model.LiquidEffect;
			this.modifierFlags = DrinkModifierFlags.None;

			this.drinkTypes.Clear();

			if (liquidEffect != null)
			{
				StartCoroutine(EmptyDrink(liquidEffect));
			}
		}

		/// <summary>
		/// Clears all currently assigned <see cref="Data.DrinkModifierFlags" /> and resets the
		/// visual state of the <see cref="World.Pickup.DrinkPickup" />.
		/// </summary>
		public void ClearModifiers()
		{
			var limeMeshRenderer = this.model.LimeMeshRenderer;
			var glassMeshRenderer = this.model.GlassMeshRenderer;

			if (limeMeshRenderer != null)
			{
				limeMeshRenderer.enabled = false;
			}

			if ((glassMeshRenderer != null) && (this.restoreMaterial != null))
			{
				glassMeshRenderer.material = this.restoreMaterial;
			}

			this.modifierFlags = DrinkModifierFlags.None;
		}

		/// <summary>
		/// Sets the <see cref="Data.DrinkModifierFlags" /> for the
		/// <see cref="World.Pickup.DrinkPickup" />.
		/// </summary>
		/// <param name="modifierFlags">Flags to enable.</param>
		/// <returns>
		/// New state of the <see cref="Data.DrinkModifierFlags" />. Compare this value with the
		/// input to confirm that all <see cref="Data.DrinkModifierFlags" /> were successfully set.
		/// </returns>
		public DrinkModifierFlags EnableModifiers(DrinkModifierFlags modifierFlags)
		{
			// TODO: A bit of questionable, nasty hardcoding that I'm not too fond of but will do
			// the job for now.
			if (modifierFlags.HasFlag(DrinkModifierFlags.Lime))
			{
				var limeMeshRenderer = this.model.LimeMeshRenderer;

				if (limeMeshRenderer != null)
				{
					limeMeshRenderer.enabled = true;
					this.modifierFlags |= DrinkModifierFlags.Lime;
				}
			}

			if (modifierFlags.HasFlag(DrinkModifierFlags.Salt))
			{
				var saltedGlassMaterial = this.model.SaltedGlassMaterial;
				var glassMeshRenderer = this.model.GlassMeshRenderer;

				if ((saltedGlassMaterial != null) && (glassMeshRenderer != null))
				{
					this.restoreMaterial = glassMeshRenderer.material;
					glassMeshRenderer.material = saltedGlassMaterial;
					this.modifierFlags |= DrinkModifierFlags.Salt;
				}
			}

			return this.modifierFlags;
		}

		/// <summary>
		/// Returns true if the <see cref="World.Pickup.DrinkPickup" /> matches the specified
		/// <see cref="Data.DrinkRecipe" />.
		/// </summary>
		/// <param name="drinkRecipe">Specified recipe.</param>
		/// <returns>
		/// <c>true</c> if the <see cref="Data.DrinkRecipe" /> matches the
		/// <see cref="World.Pickup.DrinkPickup" />, otherwise <c>false</c>.
		/// </returns>
		public bool MatchesRecipe(DrinkRecipe drinkRecipe)
		{
			if ((this.drinkGlass == drinkRecipe.drinkGlass) &&
					(this.modifierFlags == drinkRecipe.drinkModifierFlags))
			{
				// Correct drink glass and modifiers.
				var recipeDrinkTypes = drinkRecipe.drinkTypes;

				// Short-circuit the comparison by checking, before anything else, that the two
				// collections are at the very least equal lengths.
				if (this.drinkTypes.Count == recipeDrinkTypes.Length)
				{
					// Try to prove that the drinks do not match.
					var i = 0;

					// This has to be a foreach because of HashSet.
					foreach (var drinkType in this.drinkTypes)
					{
						if (recipeDrinkTypes[i] != drinkType)
						{
							return false;
						}

						i += 1;
					}

					return true;
				}
			}

			return false;
		}
	}

	#if UNITY_EDITOR
	/// <summary>
	/// Custom editor for <see cref="World.Pickup.DrinkPickup" />.
	/// </summary>
	[CustomEditor(inspectedType: typeof(DrinkPickup))]
	public sealed class DrinkPickupEditor : Editor
	{
		private const string forceInvalidateText = "Force Invalidate Model";

		private DrinkPickup pickup = null;

		private DrinkGlass itemSelected = null;

		private DrinkGlass itemLast = null;

		/// <summary>
		/// Updates the inspector GUI.
		/// </summary>
		public override void OnInspectorGUI()
		{
			this.itemLast = this.itemSelected;

			this.itemSelected = (DrinkGlass)EditorGUILayout.
					ObjectField("Drink Glass", this.itemSelected, typeof(DrinkGlass), false);

			if ((this.itemSelected != this.itemLast) || GUILayout.Button(forceInvalidateText))
			{
				// Update the assignment and force the serialized data to update.
				this.pickup.AssignDrinkGlass(this.itemSelected);
				EditorUtility.SetDirty(this.pickup);
			}
		}

		private void Awake()
		{
			this.pickup = (DrinkPickup)this.target;
			this.itemSelected = this.pickup.DrinkGlass;
		}
	}
	#endif
}
