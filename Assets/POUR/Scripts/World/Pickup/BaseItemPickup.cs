using Data;
using Data.Item;
using Model;
using UnityEditor;
using UnityEngine;
using Utility;

namespace World.Pickup
{
	/// <summary>
	/// A pickup that contains <see cref="Data.Item.BaseItem" />-derived data.
	/// </summary>
	public abstract class BaseItemPickup : BasePickup
	{
		[SerializeField]
		private BaseItem item = null;

		[SerializeField]
		private BaseItemModel model = null;

		/// <summary>
		/// <see cref="UnityEngine.Collider" /> used for pick-up interaction
		/// </summary>
		protected override Collider Collider => this.model.Collider;

		/// <summary>
		/// Currently held <see cref="Data.Item.BaseItem" />.
		///
		/// This property may be specialized by sub-classes using property shadowing.
		/// </summary>
		public BaseItem Item => this.item;

		/// <summary>
		/// <see cref="Model.Item.BaseItemModel" /> instance.
		///
		/// This property may be specialized by sub-classes using property shadowing.
		/// </summary>
		protected BaseItemModel Model => this.model;

		/// <summary>
		/// Euler angles to offset the held transform by.
		/// </summary>
		protected override Vector3 OffsetEulerAngle => this.model.OffsetEulerAngle;

		/// <summary>
		/// Position to offset the held transform by.
		/// </summary>
		protected override Vector3 OffsetPosition => this.model.OffsetPosition;

		/// <summary>
		/// Assigns the <see cref="DrinkItemPickup" /> <see cref="Data.Item" />.
		///
		/// This function works in both edit-mode as well as at runtime.
		/// </summary>
		/// <param name="newItem">Item type to assign, or <c>null</c> to clear it.</param>
		public void AssignItem(BaseItem newItem)
		{
			BaseModel.DestroyModel(this.model);

			if (newItem == null)
			{
				this.model = null;

				this.gameObject.SetActive(false);
			}
			else
			{
				var newItemType = newItem.GetType();

				if (newItemType == this.GetItemType())
				{
					this.model = BaseModel.
							InstantiateModel<BaseItemModel>(newItem.modelPrefab, this.transform);

					if (this.model != null)
					{
						var modelTransform = this.model.transform;
						modelTransform.position = this.transform.position;
						modelTransform.parent = this.transform;
					}

					this.gameObject.SetActive(true);
				}
				else
				{
					Debug.LogError(
							"Cannot assign " + newItemType.Name + " to " + this.GetType().Name);
				}
			}

			var oldItem = this.item;
			this.item = newItem;

			if (this.OutlineEffect != null)
			{
				this.OutlineEffect.UpdateRenderers();
			}

#if UNITY_EDITOR

			if (UnityEditor.EditorApplication.isPlaying)
			{
				this.OnItemChange(oldItem);
			}
#else
			this.OnItemChange(oldItem);
#endif
		}

		/// <summary>
		/// Configures the <see cref="World.Pickup.BaseItemPickup" />.
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			// Handler for pickup sound playback.
			this.OnPickedUp.AddListener((controller) =>
			{
				var interactSound = ObjectPool.GetRandomElement(this.item.interactSounds);

				if (interactSound != null)
				{
					this.PlayAudio(interactSound);
				}
			});

			// Handler for item fall damage and associated audio playback.
			this.OnLanded.AddListener((float fallDistance) =>
			{
				void PlayDropSound()
				{
					var dropSound = ObjectPool.GetRandomElement(this.item.dropSounds);

					if (dropSound != null)
					{
						this.PlayAudio(dropSound);
					}
				}

				var fallDamageDistance = this.item.fallDamageDistance;

				if (fallDamageDistance == 0.0f)
				{
					// Item cannot be destroyed by fall damage.
					PlayDropSound();
				}
				else if (fallDistance > fallDamageDistance)
				{
					// Item can be damaged by fall distance and the fall was great enough.
					var destroySound = ObjectPool.GetRandomElement(this.item.destroySounds);
					// No point calling this in the start if it's only ever going to get called
					// once.
					var particleSystem = this.GetComponentInChildren<ParticleSystem>();

					if (destroySound != null)
					{
						this.PlayAudio(destroySound);
					}

					if (particleSystem != null)
					{
						// Play the system and any of its children.
						particleSystem.Play(true);
					}

					this.model.Hide();
					this.CallDeferred(2.0f, () => Destroy(this.gameObject));
				}
				else
				{
					// Item can be damaged by fall damage but the fall distance was not great
					// enough.
					PlayDropSound();
				}
			});

			if (this.item == null)
			{
				// Be gone (for now).
				this.gameObject.SetActive(false);
			}
			else
			{
				this.OnItemChange(null);
			}
		}

		public abstract System.Type GetItemType();

		protected abstract void OnItemChange(BaseItem oldItem);
	}

	#if UNITY_EDITOR
	/// <summary>
	/// Editor user interface for <see cref="ItemPickupEditor" />s.
	/// </summary>
	[CustomEditor(inspectedType: typeof(BaseItemPickup), editorForChildClasses: true)]
	public sealed class ItemPickupEditor : Editor
	{
		private const string forceInvalidateText = "Force Invalidate Model";

		private BaseItemPickup pickup = null;

		private BaseItem itemSelected = null;

		private BaseItem itemLast = null;

		/// <summary>
		/// Updates the inspector GUI.
		/// </summary>
		public override void OnInspectorGUI()
		{
			var itemType = this.pickup.GetItemType();
			this.itemLast = this.itemSelected;

			this.itemSelected = (BaseItem)
					EditorGUILayout.ObjectField(itemType.Name, this.itemSelected, itemType, false);

			if ((this.itemSelected != this.itemLast) || GUILayout.Button(forceInvalidateText))
			{
				// Update the assignment and force the serialized data to update.
				this.pickup.AssignItem(this.itemSelected);
				EditorUtility.SetDirty(this.pickup);
			}
		}

		private void Awake()
		{
			this.pickup = (BaseItemPickup)this.target;
			this.itemSelected = this.pickup.Item;
		}
	}
	#endif
}
