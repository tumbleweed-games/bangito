using UnityEngine;
using Visual;

namespace Model.Item
{
	/// <summary>
	/// Used to provide direct access to important components on an item model prefab without
	/// needing to know any structural details, perform component lookups, or dereference.
	/// </summary>
	public class BottleItemModel : BaseItemModel
	{
		[SerializeField]
		private BottledLiquidEffect bottledLiquidEffect = null;

		[SerializeField]
		private CorkPopEffect corkPopEffect = null;

		/// <summary>
		/// Reference to the model <see cref="Visual.CorkPopEffect" />.
		/// </summary>
		public CorkPopEffect CorkPopEffect => this.corkPopEffect;

		/// <summary>
		/// Reference to the item's <see cref="Visual.BottledLiquidEffect"/>.
		/// </summary>
		public BottledLiquidEffect LiquidEffect => this.bottledLiquidEffect;
	}
}
