
namespace Model.Item
{
	/// <summary>
	/// Used to provide direct access to important components on an item model prefab without
	/// needing to know any structural details, perform component lookups, or dereference.
	/// </summary>
	public class MixerItemModel : BaseItemModel
	{

	}
}
