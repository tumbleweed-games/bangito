using UnityEngine;

namespace Model
{
	public abstract class BaseItemModel : BaseModel
	{
		[SerializeField]
		private Vector3 offsetEulerAngle = Vector3.zero;

		[SerializeField]
		private Vector3 offsetPosition = Vector3.zero;

		[SerializeField]
		private new Collider collider = null;

		/// <summary>
		/// Reference to the primary model <see cref="UnityEngine.Collider" />.
		/// </summary>
		public Collider Collider => this.collider;

		/// <summary>
		/// Offset holding rotation in euler angles.
		/// </summary>
		public Vector3 OffsetEulerAngle => this.offsetEulerAngle;

		/// <summary>
		/// Offset holding position.
		/// </summary>
		public Vector3 OffsetPosition => this.offsetPosition;
	}
}
