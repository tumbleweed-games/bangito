using UnityEngine;

namespace Model
{
	/// <summary>
	/// Used to provide direct access to important components on a character model prefab without
	/// needing to know any structural details, perform component lookups, or dereference.
	/// </summary>
	public class CharacterModel : BaseModel
	{
		[SerializeField]
		private Transform gunTransform = null;

		/// <summary>
		/// Reference to the model gun transform.
		/// </summary>
		public Transform GunTransform => this.gunTransform;
	}
}
