using UnityEditor;
using UnityEngine;

namespace Model
{
	/// <summary>
	/// Used to provide direct access to important components on a model prefab without needing to
	/// know any structural details, perform component lookups, or dereference.
	/// </summary>
	public abstract class BaseModel : MonoBehaviour
	{
		private const string objectName = "Model";

		private Renderer[] renderers;

		private void Awake()
		{
			this.renderers = this.GetComponentsInChildren<Renderer>();
		}

		/// <summary>
		/// Editor / runtime-agnostic method of destroying a <see cref="CharacterModel" />.
		/// </summary>
		/// <param name="model">Model to destroy. Passing <c>null</c> does nothing.</param>
		public static void DestroyModel(BaseModel model)
		{
			if (model != null)
			{
#if UNITY_EDITOR
				if (UnityEditor.EditorApplication.isPlaying)
				{
					// Destroy is more efficient at runtime.
					Destroy(model.gameObject);
				}
				else
				{
					// "Destroy" does not work in editor mode.
					DestroyImmediate(model.gameObject);
				}
#else
				Destroy(model.gameObject);
#endif
			}
		}

		/// <summary>
		/// Hides the visual elements of a model.
		/// </summary>
		public void Hide()
		{
			foreach (var renderer in this.renderers)
			{
				renderer.enabled = false;
			}
		}

		/// <summary>
		/// Attempts to instantiate a model prefab with <see cref="Model.BaseModel" />-derived type
		/// <c>T</c>, assigning a parent, if one is provided.
		/// </summary>
		/// <param name="modelPrefab">Model prefab to instantiate.</param>
		/// <param name="parentTransform">
		/// Parent <see cref="UnityEngine.Transform" />, or <c>null</c> for none.
		/// </param>
		/// <typeparam name="T"><see cref="Model.BaseModel" />-derived type.</typeparam>
		/// <returns>
		/// The instantiated prefab via its <see cref="Model.BaseModel" />-derived component
		/// instance, or <c>null</c> if the model prefab was invalid.
		/// </returns>
		public static T InstantiateModel<T>(GameObject modelPrefab, Transform parentTransform)
				where T : BaseModel
		{
			if (modelPrefab != null)
			{
#if UNITY_EDITOR
				if (!EditorApplication.isPlaying)
				{
					var modelInstance = (GameObject)PrefabUtility.InstantiatePrefab(modelPrefab);
					modelInstance.transform.parent = parentTransform;
					modelInstance.name = objectName;

					return modelInstance.GetComponent<T>();
				}
#endif
				var modelObject = GameObject.Instantiate(modelPrefab, parentTransform);
				modelObject.name = objectName;

				return modelObject.GetComponent<T>();
			}

			return default(T);
		}

		/// <summary>
		/// Un-hides the visual elements of a model.
		/// </summary>
		public void Show()
		{
			foreach (var renderer in this.renderers)
			{
				renderer.enabled = true;
			}
		}

		public static void ValidateModel<T>(ref GameObject modelPrefab) where T : BaseModel
		{
			if ((modelPrefab != null) && (modelPrefab.GetComponent<T>() == null))
			{
				modelPrefab = null;

				Debug.LogWarning("Model prefab must contain a " +
						typeof(T).Name + " component on the root game object");
			}
		}
	}
}
