using UnityEngine;
using Visual;

namespace Model
{
	/// <summary>
	/// Used to provide direct access to important components on a model prefab without needing to
	/// know any structural details, perform component lookups, or dereference.
	/// </summary>
	public class GlassModel : BaseModel
	{
		[SerializeField]
		private new Collider collider = null;

		[SerializeField]
		private BottledLiquidEffect liquidEffect = null;

		[SerializeField]
		private MeshRenderer glassMeshRenderer = null;

		[SerializeField]
		private MeshRenderer limeMeshRenderer = null;

		[SerializeField]
		private Material saltedGlassMaterial = null;

		/// <summary>
		/// Primary <see cref="UnityEngine.Collider" />.
		/// </summary>
		public Collider Collider => this.collider;

		/// <summary>
		/// Glass <see cref="UnityEngine.MeshRenderer" />.
		/// </summary>
		public MeshRenderer GlassMeshRenderer => this.glassMeshRenderer;

		/// <summary>
		/// Lime <see cref="UnityEngine.MeshRenderer" />.
		/// </summary>
		public MeshRenderer LimeMeshRenderer => this.limeMeshRenderer;

		/// <summary>
		/// <see cref="Visual.BottledLiquidEffect" /> liquid effect.
		/// </summary>
		public BottledLiquidEffect LiquidEffect => this.liquidEffect;

		/// <summary>
		/// Salted glass <see cref="UnityEngine.Material" />.
		/// </summary>
		public Material SaltedGlassMaterial => this.saltedGlassMaterial;
	}
}
