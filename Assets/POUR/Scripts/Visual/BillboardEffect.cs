using UnityEngine;

namespace Visual
{
	/// <summary>
	/// Applies a "DOOM-like" automatic rotating billboard effect against the target
	/// <see cref="UnityEngine.Camera" /> instance, which will default to the scene's main camera.
	/// </summary>
	public sealed class BillboardEffect : MonoBehaviour
	{
		[SerializeField]
		[Tooltip("Optional specifier for the target Camera, which will otherwise default to the main Camera.")]
		private Camera targetCamera = null;

		[SerializeField]
		[Tooltip("Optional specifier for the target Transform, which will otherwise default to host GameObject Transform.")]
		private Transform targetTransform = null;

#if UNITY_EDITOR
		private void OnValidate()
		{
			if (this.targetTransform == null)
			{
				this.targetTransform = this.transform;
			}
		}
#endif

		private void Start()
		{
			if (this.targetCamera == null)
			{
				this.targetCamera = Camera.main;
			}
		}

		private void Update()
		{
			if ((this.targetTransform != null) && (this.targetCamera != null))
			{
				this.targetTransform.rotation = this.targetCamera.transform.rotation;
			}
		}
	}
}
