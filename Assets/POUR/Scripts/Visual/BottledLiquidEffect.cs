﻿using UnityEditor;
using UnityEngine;

namespace Visual
{
	/// <summary>
	/// Applies a half-filled liquid effect to the target <see cref="UnityEngine.MeshRenderer" />
	/// instance.
	/// </summary>
	public sealed class BottledLiquidEffect : MonoBehaviour
	{
		/// <summary>
		/// Minimum fill value.
		/// </summary>
		public const float fillValueMin = 0.0f;

		/// <summary>
		/// Maximum fill value.
		/// </summary>
		public const float fillValueMax = 1.0f;

		private static int shaderFillValueId = Shader.PropertyToID("_FillValue");

		private Color color;

		[SerializeField]
		private new MeshRenderer renderer;

		[SerializeField]
		private float heightMax = 0.5f;

		[SerializeField]
		private float heightMin = -0.5f;

		[SerializeField]
		private float fillValue = 0.0f;

		/// <summary>
		/// Liquid <see cref="UnityEngine.Material" /> color tint.
		/// </summary>
		public Color Color => this.color;

		/// <summary>
		/// Liquid <see cref="UnityEngine.Material" /> fill value, represented as a normalized
		/// value between <c>0</c> and <c>1</c>.
		/// </summary>
		public float FillValue => this.fillValue;

		private void Awake()
		{
			if (this.renderer == null)
			{
				// If a renderer is not supplied then it will try and default one on the game
				// object, if it even has one.
				this.renderer = this.GetComponent<MeshRenderer>();

				if (this.renderer == null)
				{
					// Really, it's still null? PEBCAK.
					Debug.LogError(
						this.GetType().Name +
						".renderer must be assigned or have a local Component that macthes type " +
						typeof(MeshRenderer).Name
					);

					return;
				}
			}

			this.SetFill(this.fillValue);
		}

#if UNITY_EDITOR
		private void OnValidate()
		{
			this.fillValue = Mathf.Clamp(this.fillValue, fillValueMin, fillValueMax);
		}
#endif

		/// <summary>
		/// Sets the liquid <see cref="UnityEngine.Material" /> tint color.
		/// </summary>
		/// <param name="color">Liquid tint color.</param>
		public void SetColor(Color color)
		{
			var material = this.renderer.material;
			this.color = color;

			material.SetColor("_Tint", color);
			material.SetColor("_TopColor", color);
		}

		/// <summary>
		/// Sets the liquid <see cref="UnityEngine.Material" /> fill value.
		/// </summary>
		/// <param name="value">Liquid fill value.</param>
		public void SetFill(float value)
		{
			this.fillValue = Mathf.Clamp(value, fillValueMin, fillValueMax);

			void EditorImpl()
			{
				// TODO: Fix bug where editing temporary material breaks reference to original.
				var rendererMaterial = new Material(this.renderer.sharedMaterial);

				rendererMaterial.SetFloat(shaderFillValueId,
						Mathf.Lerp(this.heightMin, this.heightMax, this.fillValue));

				this.renderer.sharedMaterial = rendererMaterial;
			}

			void RuntimeImpl()
			{
				this.renderer.material.SetFloat(shaderFillValueId,
						Mathf.Lerp(this.heightMin, this.heightMax, this.fillValue));
			}

#if UNITY_EDITOR
			if (EditorApplication.isPlaying)
			{
				RuntimeImpl();
			}
			else
			{
				EditorImpl();
			}
#else
			RuntimeImpl();
#endif
		}
	}

#if UNITY_EDITOR
	/// <summary>
	/// Custom Unity inspector for <see cref="Visual.BottledLiquidEffect" />.
	/// </summary>
	[CustomEditor(typeof(BottledLiquidEffect))]
	public class BottledLiquidEffectEditor : Editor
	{
		private const string infoMessageText =
				"A target Renderer must be set before liquid properties may be altered in-editor";

		private BottledLiquidEffect effect;

		private void Awake()
		{
			this.effect = (BottledLiquidEffect)this.target;
		}

		/// <summary>
		/// Updates the inspector GUI.
		/// </summary>
		public override void OnInspectorGUI()
		{
			var rendererProperty = this.serializedObject.FindProperty("renderer");

			EditorGUILayout.PropertyField(rendererProperty);

			if (rendererProperty.objectReferenceValue != null)
			{
				var oldFillValue = this.effect.FillValue;

				var fillValue = EditorGUILayout.Slider(
					"Fill Value",
					value: oldFillValue,
					leftValue: BottledLiquidEffect.fillValueMin,
					rightValue: BottledLiquidEffect.fillValueMax
				);

				EditorGUILayout.PropertyField(this.serializedObject.FindProperty("heightMin"));
				EditorGUILayout.PropertyField(this.serializedObject.FindProperty("heightMax"));

				if ((fillValue != oldFillValue) || this.serializedObject.ApplyModifiedProperties())
				{
					this.effect.SetFill(fillValue);
					EditorUtility.SetDirty(this.target);
				}
			}
			else
			{
				EditorGUILayout.HelpBox(infoMessageText, MessageType.Info);
			}
		}
	}
#endif
}
