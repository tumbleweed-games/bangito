﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace Visual
{
	public class OutlineEffect : MonoBehaviour
	{
		/// <summary>
		/// Singleton controller instance for dispatching the post-process effects of focused
		/// <see cref="Visual.OutlineEffect" />.
		/// </summary>
		[DisallowMultipleComponent, RequireComponent(typeof(Camera))]
		private class Compositor : MonoBehaviour
		{
			private const float intensity = 2.0f;

			private static readonly int intesityID = Shader.PropertyToID("_Intensity");

			private static readonly int prePassRenderTexID = Shader.PropertyToID("_PrePassTex");

			private static readonly int blurPassRenderTexID = Shader.PropertyToID("_BlurredTex");

			private static readonly  int tempRenderTexID = Shader.PropertyToID("_TempTex0");

			private static readonly int blurSizeID = Shader.PropertyToID("_BlurSize");

			private static readonly int outlineColourID = Shader.PropertyToID("_OutlineColour");

			private Material compositeMaterial;

			/// <summary>
			/// Current <see cref="Visual.OutlineEffect" />s marked to render.
			/// </summary>
			public OutlineEffect focusedOutlineEffect;

			/// <summary>
			/// Command buffer for queueing up renderable <see cref="Visual.OutlineEffect" />s in
			/// <see cref="Visual.OutlineEffect.EffectController.enabledOutlineEffects" />.
			/// </summary>
			public CommandBuffer commandBuffer;

			private Material outlineMaterial;

			private Material blurMaterial;

			private void Awake()
			{
				this.outlineMaterial = new Material(Shader.Find("Hidden/OutlineShader"));
				this.blurMaterial = new Material(Shader.Find("Hidden/Blur"));
				this.compositeMaterial = new Material(Shader.Find("Hidden/OutlineComposite"));

				compositeMaterial.SetFloat(intesityID, intensity);

				this.commandBuffer = new CommandBuffer()
				{
					name = "OutlineEffectControllerCommandBuffer"
				};
			}

			private void OnDestroy()
			{
				// Clean up non-GC resources.
				this.commandBuffer.Dispose();
			}

			private void OnRenderImage(RenderTexture source, RenderTexture destination)
			{
				Graphics.Blit(source, destination, compositeMaterial, 0);
			}

			private void Update()
			{
				// Quicker than division by 2.
				var halfScreenWidth = (Screen.width >> 1);
				var halfScreenHeight = (Screen.height >> 1);

				// Clear existing commands
				this.commandBuffer.Clear();

				// Retrieve a copy of the screen to store in the prepass shader property.
				this.commandBuffer.GetTemporaryRT(
					prePassRenderTexID,
					Screen.width,
					Screen.height,
					0,
					FilterMode.Bilinear,
					RenderTextureFormat.ARGB32,
					RenderTextureReadWrite.Default,
					QualitySettings.antiAliasing
				);

				// Set that as active render target and clear it.
				this.commandBuffer.SetRenderTarget(prePassRenderTexID);
				this.commandBuffer.ClearRenderTarget(true, true, Color.clear);

				if (this.focusedOutlineEffect)
				{
					// Set the shaders outline colour to the objects colour. This means we have one
					// shader that we simply change the colour of for each object.
					this.commandBuffer.SetGlobalColor(
							outlineColourID, this.focusedOutlineEffect.currentColour);

					foreach (var renderer in this.focusedOutlineEffect.renderers)
					{
						// Don't want to draw renderers marked as disabled.
						if (renderer.enabled)
						{
							// Loop through renderers and draw them with the outline.
							this.commandBuffer.DrawRenderer(renderer, outlineMaterial);
						}
					}
				}

				// Save 2 copies of the screen at a reduced size for efficiency.
				// Blurring can be done at a reduced scale as image quality doesnt exactly matter.
				this.commandBuffer.GetTemporaryRT(blurPassRenderTexID,
						halfScreenWidth, halfScreenHeight, 0, FilterMode.Bilinear);

				this.commandBuffer.GetTemporaryRT(tempRenderTexID,
						halfScreenWidth, halfScreenHeight, 0, FilterMode.Bilinear);

				// Copy the prepass onto the blur pass texture.
				this.commandBuffer.Blit(prePassRenderTexID, blurPassRenderTexID);

				// Set the texel size in the shader.
				this.commandBuffer.SetGlobalVector(blurSizeID, new Vector2
				{
					x = (1.5f / halfScreenWidth),
					y = (1.5f / halfScreenHeight)
				});

				// Repeatedly blit to blur the image.
				for (int i = 0; i < 4; i += 1)
				{
					this.commandBuffer.Blit(blurPassRenderTexID, tempRenderTexID, blurMaterial, 0);
					this.commandBuffer.Blit(tempRenderTexID, blurPassRenderTexID, blurMaterial, 1);
				}
			}
		}

		private const float fadeInRate = 0.12f;

		private const float fadeOutRate = 0.24f;

		private const float fadeSecondStep = 0.05f;

		private static Compositor globalCompositor;

		private Color currentColour = Color.black;

		[SerializeField]
		private Color outlineColour = new Color(0, 124, 180);

		private Renderer[] renderers;

		private void Start()
		{
			// TODO: Will this cause issues when changing scenes as the instance may be destroyed
			// but the reference to it will remain non-null.
			if (globalCompositor == null)
			{
				// Add the Compositor component to the main camera if it does not already have it.
				var mainCamera = Camera.main;

				if ((mainCamera != null) && (mainCamera.GetComponent<Compositor>() == null))
				{
					// This all breaks if there is no main camera in the scene.
					globalCompositor = mainCamera.gameObject.AddComponent<Compositor>();

					mainCamera.AddCommandBuffer(
						CameraEvent.BeforeImageEffects,
						globalCompositor.commandBuffer
					);
				}
			}

			this.UpdateRenderers();
		}

		/// <summary>
		/// Focuses on the outline effect, removing focus from the previous and gradually fading in
		/// the new focus.
		/// </summary>
		public static void Focus(OutlineEffect outlineEffect)
		{
			System.Collections.IEnumerator FadeIn()
			{
				var rate = 0.0f;

				while (rate < 1.0f)
				{
					outlineEffect.currentColour = Color.Lerp(
							outlineEffect.currentColour, outlineEffect.outlineColour, rate);

					rate += fadeInRate;

					yield return Utility.Wait.RealtimeSeconds(fadeSecondStep);
				}
			}

			if (globalCompositor != null)
			{
				globalCompositor.focusedOutlineEffect = outlineEffect;

				if (outlineEffect != null)
				{
					outlineEffect.currentColour = Color.black;

					outlineEffect.StartCoroutine(FadeIn());
				}
			}
		}

		public void UpdateRenderers()
		{
			this.renderers = this.GetComponentsInChildren<Renderer>();
		}
	}
}
