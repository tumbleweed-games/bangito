using UnityEngine;
using Utility;

namespace Visual
{
	/// <summary>
	/// Applies a "pop-off" effect to a game object when enabled, projecting it through the air,
	/// and re-attaching it once it is disabled.
	/// </summary>
	[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
	public class CorkPopEffect : MonoBehaviour
	{
		[SerializeField]
		private Material trailMaterial = null;

		private MeshRenderer meshRenderer;

		private MeshFilter meshFilter;

		private void Awake()
		{
			this.meshRenderer = this.GetComponent<MeshRenderer>();

			if (this.meshRenderer == null)
			{
				Debug.LogError("Unable to acquire MeshRenderer");
			}

			this.meshFilter = this.GetComponent<MeshFilter>();

			if (this.meshFilter == null)
			{
				Debug.LogError("Unable to acquire MeshFilter");
			}
		}

		/// <summary>
		/// Disables the effect, resetting the cork state.
		/// </summary>
		public void Disable()
		{
			this.meshRenderer.enabled = true;
		}

		/// <summary>
		/// Enables the effect, popping the cork and projecting it through the air.
		/// </summary>
		public void Enable()
		{
			var corkObject = new GameObject("CorkPop");

			if (corkObject != null)
			{
				var corkMeshRenderer = corkObject.AddComponent<MeshRenderer>();
				var corkMeshFilter = corkObject.AddComponent<MeshFilter>();
				var corkCollider = corkObject.AddComponent<BoxCollider>();
				var corkRigidbody = corkObject.AddComponent<Rigidbody>();
				var corkLifespan = corkObject.AddComponent<Lifetime>();

				if (corkMeshRenderer != null)
				{
					corkMeshRenderer.material = this.meshRenderer.material;
				}

				if (corkMeshFilter != null)
				{
					corkMeshFilter.mesh = this.meshFilter.mesh;
				}

				if (corkCollider != null)
				{
					corkCollider.center = Vector3.zero;
					corkCollider.size = new Vector3(0.15f, 0.15f, 0.15f);
				}

				if (corkRigidbody != null)
				{
					corkRigidbody.velocity += new Vector3(
						Random.Range(0.0f, 0.15f),
						Random.Range(0.0f, 0.5f),
						Random.Range(0.0f, 0.15f)
					);
				}

				if (corkLifespan != null)
				{
					corkLifespan.Lifespan = 15f;
				}

				if (this.trailMaterial != null)
				{
					var corkTrailRenderer = corkObject.AddComponent<TrailRenderer>();

					if (corkTrailRenderer != null)
					{
						corkTrailRenderer.widthCurve = new AnimationCurve(new Keyframe[]
						{
							new Keyframe(0.0f, 0.10f),
							new Keyframe(1.0f, 0.0f)
						});

						corkTrailRenderer.time = 0.5f;
						corkTrailRenderer.emitting = true;
						corkTrailRenderer.material = trailMaterial;

						corkTrailRenderer.shadowCastingMode =
								UnityEngine.Rendering.ShadowCastingMode.Off;

						corkTrailRenderer.receiveShadows = true;
					}
				}

				corkObject.transform.CopyTransformOf(this.transform);
			}

			this.meshRenderer.enabled = false;
		}
	}
}
