
namespace Data
{
	/// <summary>
	/// Runtime-static pool of <see cref="Data.Character" />s to select from.
	/// </summary>
	[UnityEngine.CreateAssetMenu(fileName = "Character Pool", menuName = "Pool/Characters")]
	public class CharacterPool : ObjectPool<Character>
	{

	}
}
