using Model;
using UnityEngine;

namespace Data
{
	/// <summary>
	/// Represents a drinking glass.
	/// </summary>
	[CreateAssetMenu(fileName = "Drink Glass", menuName = "Drink Glass")]
	public class DrinkGlass : ScriptableObject
	{
		/// <summary>
		/// Prefab used to represent the <see cref="Data.DrinkGlass" />.
		///
		/// The editor will require it to have a <see cref="Model.GlassModel" /> component on the
		/// root game object.
		/// </summary>
		public GameObject modelPrefab = null;

#if UNITY_EDITOR
		private void OnValidate()
		{
			GlassModel.ValidateModel<GlassModel>(ref this.modelPrefab);
		}
#endif
	}
}
