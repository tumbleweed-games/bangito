using Model.Item;
using UnityEngine;
using Visual;

namespace Data.Item
{
	/// <summary>
	/// Holds template information for bottle item instances in worldspace.
	/// </summary>
	[CreateAssetMenu(fileName = "Bottle Item", menuName = "Item/Bottle")]
	public class BottleItem : BaseItem
	{
		public DrinkType drinkType = null;

		/// <summary>
		/// Fill value used while a bottle is being poured out.
		/// </summary>
		public float pouringFillValue = 1.0f;

		public AudioClip pouringSound = null;

#if UNITY_EDITOR
		/// <summary>
		/// Validates that the assigned model prefab contains a
		/// <see cref="Model.Item.BottleItemModel" /> on the root game object.
		/// </summary>
		protected override void OnValidate()
		{
			BottleItemModel.ValidateModel<BottleItemModel>(ref this.modelPrefab);

			this.pouringFillValue = Mathf.Clamp(
				this.pouringFillValue,
				BottledLiquidEffect.fillValueMin,
				BottledLiquidEffect.fillValueMax
			);
		}
#endif
	}
}
