using UnityEngine;

namespace Data.Item
{
	/// <summary>
	/// Holds template information for item instances in worldspace.
	/// </summary>
	public abstract class BaseItem : ScriptableObject
	{
		public GameObject modelPrefab;

		/// <summary>
		/// Item name used in the user interface.
		/// </summary>
		public string itemName = "";

		/// <summary>
		/// Icon sprite used in the user interface.
		/// </summary>
		public Sprite iconSprite = null;

		/// <summary>
		/// Distance at which fall damage is encountered. A value of <c>0.0f</c> prevents any
		/// falling damage from occuring.
		/// </summary>
		public float fallDamageDistance = 0.0f;

		/// <summary>
		/// Sound played upon interacting with the <see cref="Data.Item" />.
		/// </summary>
		public AudioClipPool interactSounds = null;

		/// <summary>
		/// Sound played upon destroying the <see cref="Data.Item" />.
		/// </summary>
		public AudioClipPool destroySounds = null;

		/// <summary>
		/// Sound played upon the <see cref="Data.Item" /> being dropped by a
		/// <see cref="PlayerController" />.
		/// </summary>
		public AudioClipPool dropSounds = null;

#if UNITY_EDITOR
		protected abstract void OnValidate();
#endif
	}
}
