using Model.Item;
using UnityEngine;

namespace Data.Item
{
	/// <summary>
	/// Holds template information for mixer item instances in worldspace.
	/// </summary>
	[CreateAssetMenu(fileName = "Mixer Item", menuName = "Item/Mixer")]
	public class MixerItem : BaseItem
	{
		/// <summary>
		/// Used as a low-overhead way of flagging modifiers used by the
		/// <see cref="Data.Item.MixerItem" />.
		/// </summary>
		public DrinkModifierFlags modifierFlags;

		/// <summary>
		/// Amount of uses a <see cref="Data.Item.MixerItem" /> has before it is destroyed.
		///
		/// A value of <c>0</c> indicates it is an infinite resource.
		/// </summary>
		public int uses = 0;

#if UNITY_EDITOR
		/// <summary>
		/// Validates that the assigned model prefab contains a
		/// <see cref="Model.Item.MixerItemModel" /> on the root game object.
		/// </summary>
		protected override void OnValidate()
		{
			MixerItemModel.ValidateModel<MixerItemModel>(ref this.modelPrefab);
		}
#endif
	}
}
