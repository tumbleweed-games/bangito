using UnityEngine;

namespace Data
{
	/// <summary>
	/// Description information for a type of drink.
	/// </summary>
	[CreateAssetMenu(fileName = "Drink Recipe", menuName = "Drink Recipe")]
	public class DrinkRecipe : ScriptableObject
	{
		/// <summary>
		/// Drink name used in the user interface.
		/// </summary>
		public string drinkName;

		/// <summary>
		/// Icon sprite used in the user interface.
		/// </summary>
		public Sprite iconSprite = null;

		/// <summary>
		/// <see cref="Data.DrinkGlass" /> required for the drink to be poured into.
		/// </summary>
		public DrinkGlass drinkGlass;

		/// <summary>
		/// Used as a low-overhead way of flagging modifiers applied to the
		/// <see cref="Data.DrinkRecipe" />.
		/// </summary>
		public DrinkModifierFlags drinkModifierFlags;

		public DrinkType[] drinkTypes = {};

		public int score = 100;
	}

	/// <summary>
	/// Used as a low-overhead way of flagging modifiers about a drink.
	/// </summary>
	[System.Flags]
	public enum DrinkModifierFlags
	{
		None = 0,
		Lime = 1,
		Salt = 2
	}
}
