using Model;
using UnityEngine;

namespace Data
{
	/// <summary>
	/// Class that represents an <see cref="Actor" />'s character and attributes.
	/// </summary>
	[CreateAssetMenu(fileName = "Character", menuName = "Character")]
	public class Character : ScriptableObject
	{
		/// <summary>
		/// Fallback value when no other is available.
		/// </summary>
		public const int defaultHealth = 10;

		private const int maxAttribute = 10;

		private const int minHealth = 1;

		private const int minAttribute = 1;

		public GameObject modelPrefab = null;

		/// <summary>
		/// Character name / title.
		/// </summary>
		public string title = "";

		/// <summary>
		/// Maximum health.
		/// </summary>
		public int maxHealth = 10;

		/// <summary>
		/// Modifier flags that change how and what the player should serve the
		/// <see cref="Data.Character" />.
		/// </summary>
		public CharacterModifierFlags modifierFlags;

		/// <summary>
		/// Derived character attribute used to derive concrete attributes.
		/// </summary>
		public byte patience;

		public DrinkRecipePool preferredDrinkRecipes = null;

		public float dialogPanelYOffset = 2.5f;

		/// <summary>
		/// Sound emitted when the <see cref="Data.Character" /> is happy.
		/// </summary>
		public AudioClip happySound = null;

		/// <summary>
		/// Sound emitted when the <see cref="Data.Character" /> is indifferent.
		/// </summary>
		public AudioClip normalSound = null;

		/// <summary>
		/// Sound emitted when the <see cref="Data.Character" /> is angry.
		/// </summary>
		public AudioClip angrySound = null;

#if UNITY_EDITOR
		private void OnValidate()
		{
			BaseModel.ValidateModel<CharacterModel>(ref this.modelPrefab);

			// Maximum health cannot be less than minimum health.
			if (this.maxHealth < minHealth)
			{
				this.maxHealth = minHealth;
			}

			if (this.patience < minAttribute)
			{
				this.patience = minAttribute;
			}
			else if (this.patience > maxAttribute)
			{
				this.patience = maxAttribute;
			}
		}
#endif
	}

	public enum CharacterModifierFlags
	{
		None,
		Underage
	}
}
