
namespace Data
{
	/// <summary>
	/// Runtime-static pool of <see cref="Data.Drink" />s to select from.
	/// </summary>
	[UnityEngine.CreateAssetMenu(fileName = "Audio Clip Pool", menuName = "Pool/Audio Clip")]
	public class AudioClipPool : ObjectPool<UnityEngine.AudioClip>
	{

	}
}
