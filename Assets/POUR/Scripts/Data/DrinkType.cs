using UnityEngine;

namespace Data
{
	/// <summary>
	/// Description information for a type of drink.
	/// </summary>
	[CreateAssetMenu(fileName = "Drink Type", menuName = "Drink Type")]
	public class DrinkType : ScriptableObject
	{
		/// <summary>
		/// Color of the liquid in when in <see cref="World.Pickup.BottleItemPickup" />s,
		/// <see cref="World.Pickup.DrinkPickup" />s, and when poured.
		/// </summary>
		public Color32 liquidColor;

		/// <summary>
		/// Amount of alcohol contained in the drink, represented as a value between <c>0</c> and
		/// <c>1</c>, with <c>0</c> being nonone and <c>1</c> basically being pure ethanol.
		/// </summary>
		public float ethanolVolume;

#if UNITY_EDITOR
		private void OnValidate()
		{
			this.ethanolVolume = Mathf.Clamp01(this.ethanolVolume);
		}
#endif
	}
}
