
namespace Data
{
	/// <summary>
	/// Drink recipe pool.
	/// </summary>
	[UnityEngine.CreateAssetMenu(fileName = "Drink Recipe Pool", menuName = "Pool/Drink Recipe")]
	public class DrinkRecipePool : ObjectPool<Data.DrinkRecipe>
	{

	}
}
