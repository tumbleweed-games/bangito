using UnityEngine;
using Utility;

namespace Data
{
	/// <summary>
	/// A runtime-static pool of T that can be selected from.
	/// </summary>
	public class ObjectPool<T> : ScriptableObject where T : Object
	{
		[SerializeField]
		private T[] elements;

		/// <summary>
		/// Elements to select from.
		/// </summary>
		public ReadOnlySlice<T> Elements => this.elements;

#if UNITY_EDITOR
		/// <summary>
		/// Provides in-editor validation for <see cref="Data.ScriptableObjectPool{T}" />.
		/// </summary>
		protected virtual void OnValidate()
		{
			if (this.elements == null)
			{
				this.elements = new T[0];
			}
		}
#endif
	}

	/// <summary>
	/// Implements utility functions for <see cref="Data.ObjectPool{T}" /> with a level of safety
	/// that would otherwise not be achievable using regular member functions that guarantee no
	/// amount of safety while using values which have the potential to be <c>null</c>.
	/// </summary>
	public static class ObjectPool
	{
		/// <summary>
		/// Returns a random element, or <c>null</c> if the pool is empty.
		///
		/// Note that sequential calls may return duplicate elements. This is because the random
		/// element is not predicated on any previous results and does not track any state beyond
		/// that of the random number generator.
		/// </summary>
		/// <returns>Random element, or <c>null</c> if the pool is empty.</returns>
		public static T GetRandomElement<T>(ObjectPool<T> pool) where T : Object
		{
			return ((pool == null) || (pool.Elements.Length == 0) ?
					null : pool.Elements[Random.Range(0, pool.Elements.Length)]);
		}

		/// <summary>
		/// Returns an array of random elements, or an empty array if either amount is less than
		/// <c>1</c> or the pool is empty.
		///
		/// Note that the returned array may contain duplicate items. This is because the random
		/// elements in the array are not predicated on the previous element that was added, and
		/// the only state tracked is that of the random number generator itself.
		/// </summary>
		/// <param name="amount">Number or random <see cref="Data.Item" />s to return.</param>
		/// <returns>Array of random elements, or an empty array if either amount is less than
		/// <c>1</c> or the pool is empty</returns>
		public static T[] GetRandomElements<T>(ObjectPool<T> pool, int amount) where T : Object
		{
			if (amount > 0)
			{
				T[] randoms = new T[amount];

				for (var i = 0; i < amount; i += 1)
				{
					randoms[i] = GetRandomElement(pool);
				}

				return randoms;
			}

			return new T[0];
		}
	}
}
