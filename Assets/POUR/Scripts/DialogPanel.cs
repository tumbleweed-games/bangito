﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Handles the rendering of images, text, and symbols on a flat, 2-dimensional plane.
///
/// Typically, this is used to convey contextual information to the player about the desires or
/// requirements of an Actor or other interactable game object.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public sealed class DialogPanel : MonoBehaviour
{
	[SerializeField]
	private Sprite[] backgroundSprites;

	[SerializeField]
	private SpriteRenderer background;

	[SerializeField]
	private SpriteRenderer foreground;

	/// <summary>
	/// Hides the <see cref="DialogPanel" />, disabling its <see cref="UnityEngine.GameObject" />.
	/// </summary>
	public void Hide()
	{
		this.background.gameObject.SetActive(false);
		this.foreground.gameObject.SetActive(false);
	}

	/// <summary>
	/// Shows the <see cref="DialogPanel" />, erasing anything that was previously on it in favor
	/// of whatever is passed through the function.
	/// </summary>
	/// <param name="sprite">Sprite to render.</param>
	/// <param name="color">Sprite tint color.</param>
	public void ShowSprite(Sprite sprite, Color color)
	{
		this.foreground.sprite = sprite;
		this.foreground.color = color;
		this.background.gameObject.SetActive(true);
		this.foreground.gameObject.SetActive(true);

		GetComponent<Animator>().Play(0);
	}

	private void Awake()
	{
		this.background.sprite =
			this.backgroundSprites[Random.Range(0, this.backgroundSprites.Length)];
	}
}
