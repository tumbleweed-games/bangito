using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using World.Spawning;
using UnityEngine.SceneManagement;

/// <Summary>
/// Manages the game session state.
/// </Summary>
public class GameManager : MonoBehaviour
{
	/// <summary>
	/// Event that requires a boolean value.
	/// </summary>
	public class UnityBoolEvent : UnityEvent<bool> { }

	/// <Summary>
	/// Provides singleton access to the scene <see cref="GameManager"> instance.
	/// </Summary>
	public static GameManager Instance { get; private set; } = null;

	/// <summary>
	/// Public accessor for if the game is playing;
	/// </summary>
	public bool IsPlaying
	{
		get { return this.isPlaying; }
		private set
		{
			if(this.isPlaying != value)
			{
				this.isPlaying = value;
				this.onPause.Invoke(value);
			}
		}
	}

	/// <summary>
	/// Event callback for score change.
	/// </summary>
	public UnityEvent OnScoreChange => this.onScoreChange;

	[SerializeField]
	private UnityEvent onScoreChange = new UnityEvent();

	/// <summary>
	/// Event callback for lives change.
	/// </summary>
	public UnityEvent OnDeductLives => this.onDeductLives;

	[SerializeField]
	private UnityEvent onDeductLives = new UnityEvent();

	/// <summary>
	/// Event callback for lives change.
	/// </summary>
	public UnityEvent OnRestoreLives => this.onRestoreLives;

	[SerializeField]
	private UnityEvent onRestoreLives = new UnityEvent();

	/// <summary>
	/// Event callback for timer change.
	/// </summary>
	public UnityEvent OnTimerChange => this.onTimerChange;

	[SerializeField]
	private UnityEvent onTimerChange = new UnityEvent();

	/// <summary>
	/// Event callback for pasuing the game.
	/// </summary>
	public UnityBoolEvent OnPause => this.onPause;

	[SerializeField]
	private UnityBoolEvent onPause = new UnityBoolEvent();

	/// <summary>
	/// Event callback for successful completion of game.
	/// </summary>
	public UnityEvent OnSuccess => this.onSuccess;

	[SerializeField]
	private UnityEvent onSuccess = new UnityEvent();

	/// <summary>
	/// Event callback for failure state.
	/// </summary>
	public UnityEvent OnFailure => this.onFailure;

	[SerializeField]
	private UnityEvent onFailure = new UnityEvent();

	/// <Summary>
	/// The maximum number of lives that a player can have.
	/// </Summary>
	[SerializeField]
	private static int LivesMax => 3;

	/// <summary>
	/// Current number of lives.
	/// </summary>
	public int CurrentLives
	{
		get => currentLives;
	}

	private int currentLives = LivesMax;

	[SerializeField]
	private AudioClip musicClip = null;

	[SerializeField]
	private int gameDuration = 300;

	private int currentScore = 0;

	private int timeLeftToPlay = 0;

	private WaitForSeconds secondWait = new WaitForSeconds(1);

	private string timestamp = null;

	private List<ActorSpawner> actorSpawners = new List<ActorSpawner>();

	private List<BasePickupSpawner> pickupSpawners = new List<BasePickupSpawner>();

	private bool isPlaying;

	[SerializeField]
	private AudioSource audioSource = null;

	/// <summary>
	/// Read-only accessor to the duration of the game.
	/// </summary>
	public int Duration
	{
		get => this.gameDuration;
	}

	/// <Summary>
	/// Modifies the current player score and invokes any callbacks attached to
	/// <see cref="GameManager.onScoreChange">.
	/// </Summary>
	public int Score
	{
		get => this.currentScore;

		set
		{
			this.currentScore = value;

			if (this.currentScore < 0)
			{
				this.currentScore = 0;
			}

			this.onScoreChange.Invoke();
		}
	}

	/// <Summary>
	/// Indicates the time left in the level
	/// </Summary>
	public int TimeLeftToPlay
	{
		get => this.timeLeftToPlay;

		set
		{
			this.timeLeftToPlay = value;

			this.onTimerChange.Invoke();
		}
	}

	/// <summary>
	/// Retrieves a string of the current time in MM:SS.
	/// </summary>
	public string Timestamp
	{
		get
		{
			float minutes = this.TimeLeftToPlay / 60;
			float seconds = this.TimeLeftToPlay % 60;
			if (seconds < 10)
			{
				timestamp = (minutes + ":0" + seconds);
			}
			else
			{
				timestamp = (minutes + ":" + seconds);
			}

			return timestamp;
		}
	}

	/// <Summary>
	/// Subtracts player lives.
	/// </Summary>
	/// <Param name="value">Number of lives to deduct by.</param>
	/// <Returns>True if the deduction is not fatal, otherwise false if no lives remain.</Returns>
	public bool DeductLives(int value)
	{
		if (value > this.currentLives || this.currentLives - value <= 0)
		{
			this.currentLives = 0;

			this.StopAllCoroutines();
			this.onDeductLives.Invoke();
			this.onFailure.Invoke();
			this.Pause();

			return false;
		}

		this.currentLives -= value;

		this.onDeductLives.Invoke();

		return true;
	}

	/// <Summary>World.Spawningore by.</Param>
	public void RestoreLives(int value)
	{
		if ((this.currentLives + value) > LivesMax)
		{
			this.currentLives = LivesMax;
		}
		else
		{
			this.currentLives += value;
		}
		this.onRestoreLives.Invoke();
	}

	/// <Summary>
	/// Resets the game session.
	/// </Summary>
	public void Reset()
	{
		this.Score = 0;
		this.TimeLeftToPlay = this.gameDuration;
		this.RestoreLives(LivesMax);
		this.StopAllCoroutines();

		Time.timeScale = 1f;
	}

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			this.TimeLeftToPlay = this.gameDuration;
			if (this.isPlaying)
			{
				this.StartPlaying();
			}
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	/// <summary>
	/// Public method to tell the GameManager to begin the game loop.
	/// </summary>
	public void StartPlaying()
	{
		this.Resume();

		foreach (var actorSpawner in FindObjectsOfType<World.Spawning.ActorSpawner>())
		{
			actorSpawner.enabled = true;

			this.actorSpawners.Add(actorSpawner);
			actorSpawner.ImmediateSpawn();
		}

		foreach (var pickupSpawner in FindObjectsOfType<BasePickupSpawner>())
		{
			this.pickupSpawners.Add(pickupSpawner);
			pickupSpawner.ImmediateSpawn();
		}

		if (this.musicClip != null)
		{
			this.audioSource.loop = true;
			this.audioSource.clip = this.musicClip;

			this.audioSource.Play();
		}

		this.StartCoroutine(TimeManager());
	}

	/// <summary>
	/// Public method to tell the GameManager to pause the game loop.
	/// </summary>
	public void Pause()
	{
		this.IsPlaying = false;

		// Set timescale to 0 as this will pause most functions that rely on time.
		// specific things that fall outside of this can be dealt with.
		Time.timeScale = 0f;

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	public void PlaySound(AudioClip audioClip)
	{
		if (this.audioSource != null)
		{
			this.audioSource.Stop();
			this.audioSource.PlayOneShot(audioClip);
		}
	}

	/// <summary>
	/// Public method to tell the GameManager to resume the game loop
	/// </summary>
	public void Resume()
	{
		this.IsPlaying = true;
		Time.timeScale = 1f;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	/// <summary>
	/// Tell the GameManager to quit to the menu by reloading the scene.
	/// </summary>
	public void QuitToMenu()
	{
		this.Reset();
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	/// <summary>
	/// Quit the application
	/// </summary>
	public void QuitToDesktop()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	private IEnumerator TimeManager()
	{
		while (this.TimeLeftToPlay > 0)
		{
			this.TimeLeftToPlay -= 1;

			yield return secondWait;
		}

		this.onSuccess.Invoke();
		this.Pause();
	}
}
