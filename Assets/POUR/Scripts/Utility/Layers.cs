using UnityEngine;

namespace Utility
{
	/// <summary>
	/// Static utility class for accessing layer masks defined by POUR.
	/// </summary>
	public static class Layers
	{
		/// <summary>
		/// Provides information about a Unity Engine layer.
		/// </summary>
		public struct LayerInfo
		{
			/// <summary>
			/// Layer ordinal used in OnCollision events and such.
			/// </summary>
			public int Ordinal
			{
				get;
				set;
			}

			/// <summary>
			/// Layer bit mask used for efficient collision matching.
			/// </summary>
			public int Mask
			{
				get;
				set;
			}

			/// <summary>
			/// Constructs a <see cref="Utility.Layers.LayerInfo" /> using info derived from the
			/// supplied name.
			/// </summary>
			/// <param name="name">Supplied layer name.</param>
			/// <returns>Valid layer info if the layer exists, or invalid info otherwise.</returns>
			public static LayerInfo Of(string name)
			{
				return new LayerInfo
				{
					Ordinal = LayerMask.NameToLayer(name),
					Mask = LayerMask.GetMask(name)
				};
			}
		}

		public static LayerInfo Default
		{
			get;
		} = LayerInfo.Of("Default");

		/// <summary>
		/// Layer mask used for objects that are able to be picked by the player character.
		/// </summary>
		public static LayerInfo Pickups
		{
			get;
		} = LayerInfo.Of("Pickup");

		public static LayerInfo IgnorePouring
		{
			get;
		} = LayerInfo.Of("Ignore Pouring");
	}
}
