using UnityEngine;

namespace Utility
{
	/// <summary>
	/// Manages the lifespan of a <see cref="UnityEngine.GameObject" />, destroying it after an
	/// arbitrary amount of time has elapsed.
	/// </summary>
	public sealed class Lifetime : MonoBehaviour
	{
		[SerializeField]
		private float lifespan = 0.0f;

		/// <summary>
		/// Remaining lifespan in seconds.
		/// </summary>
		public float Lifespan
		{
			get => this.lifespan;
			set => this.lifespan = value;
		}

		private void Update()
		{
			this.lifespan -= Time.deltaTime;

			if (this.lifespan <= 0.0f)
			{
				Destroy(this.gameObject);
			}
		}
	}
}
