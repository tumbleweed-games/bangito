using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
	/// <summary>
	/// Unity coroutines can be extremely expensive over moderate to long execution periods. This
	/// is because they heap allocate class instances that derive from
	/// <see cref="UnityEngine.CustomYieldInstruction" />.
	///
	/// In order to circumvent this problem, this class exists to contain pre-allocated instances
	/// of the many <see cref="UnityEngine.CustomYieldInstruction" /> for use in coroutines and
	/// other iterable functions.
	/// </summary>
	public static class Wait
	{
		/// <summary>
		/// Yield Instruction for a single frame.
		/// </summary>
		public static WaitForEndOfFrame ForEndOfFrame
		{
			get;
		} = new WaitForEndOfFrame();

		/// <summary>
		/// Yield instruction for a single second of real time.
		/// </summary>
		public static WaitForSecondsRealtime RealtimeSecond
		{
			get;
		} = new WaitForSecondsRealtime(1);

		private static Dictionary<float, WaitForSeconds> secondsCache =
				new Dictionary<float, WaitForSeconds>(100);

		private static Dictionary<float, WaitForSecondsRealtime> realtimeSecondsCache =
				new Dictionary<float, WaitForSecondsRealtime>(100);

		/// <summary>
		/// Attempts to retrieve a cached instance of
		/// <see cref="UnityEngine.WaitForSecondsRealtime" /> for the specified seconds timestep,
		/// instantiating a new one if one does not already exist.
		/// </summary>
		/// <param name="seconds">Amount of real-time seconds for the instance to wait for.</param>
		/// <returns>A cached <see cref="UnityEngine.WaitForSecondsRealtime" /> instance.</returns>
		public static WaitForSecondsRealtime RealtimeSeconds(float seconds)
		{
			WaitForSecondsRealtime wait;

			if (!realtimeSecondsCache.TryGetValue(seconds, out wait))
			{
				wait = new WaitForSecondsRealtime(seconds);
			}

			return wait;
		}

		public static WaitForSeconds Seconds(float seconds)
		{
			WaitForSeconds wait;

			if (!secondsCache.TryGetValue(seconds, out wait))
			{
				wait = new WaitForSeconds(seconds);
			}

			return wait;
		}
	}
}
