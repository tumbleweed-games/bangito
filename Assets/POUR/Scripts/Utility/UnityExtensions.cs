using UnityEngine;

namespace Utility
{
	/// <summary>
	/// Helper functions for interfacing with the Unity API.
	/// </summary>
	public static class UnityExtensions
	{
		/// <summary>
		/// Callback used for deferred calls.
		/// </summary>
		public delegate void DeferredCallback();

		/// <summary>
		/// Copies the source <see cref="UnityEngine.Transform" /> values into the destination
		/// <see cref="UnityEngine.Transform" />.
		/// </summary>
		/// <param name="destinationTransform">
		/// Destination <see cref="UnityEngine.Transform" />.
		/// </param>
		/// <param name="sourceTransform">Source <see cref="UnityEngine.Transform" />.</param>
		public static void CopyTransformOf(
				this Transform destinationTransform, Transform sourceTransform)
		{
			destinationTransform.position = sourceTransform.position;
			destinationTransform.rotation = sourceTransform.rotation;
			destinationTransform.localScale = sourceTransform.lossyScale;
		}

		/// <summary>
		/// Defers the passed lambda call by a set duration, returning a handle to the delay
		/// <see cref="UnityEngine.Coroutine" />.
		/// </summary>
		/// <param name="component">Subject <see cref="UnityEngine.MonoBehaviour" /></param>
		/// <param name="duration">Number of seconds to delay the call by.</param>
		/// <param name="callback">Call to invoke.</param>
		/// <returns>Delay handle.</returns>
		public static Coroutine CallDeferred(
				this MonoBehaviour monoBehaviour, float duration, DeferredCallback callback)
		{
			System.Collections.IEnumerator Defer()
			{
				yield return Wait.Seconds(duration);

				callback.Invoke();
			}

			return monoBehaviour.StartCoroutine(Defer());
		}
	}
}
