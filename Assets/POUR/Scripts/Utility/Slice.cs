using System.Collections;
using System.Collections.Generic;

namespace Utility
{
	/// <summary>
	/// Read-only reference to a linear block of memory of type <c>T</c>.
	///
	/// Unlike regular C# arrays, <see cref="Utility.ReadOnlySlice{T}" /> always contains a valid
	/// "Length" property that may be read from even when it is "<c>null</c>".
	/// </summary>
	public struct ReadOnlySlice<T> : IEnumerable<T>
	{
		private readonly T[] array;

		/// <summary>
		/// Returns the number of elements in the internal reference.
		///
		/// A length of zero can indicate that the internal reference contains <c>0</c> items or
		/// that said reference is not currently set to a valid value.
		/// </summary>
		public int Length => ((this.array == null) ? 0 : this.array.Length);

		/// <summary>
		/// Accesses the element at <c>i<c/>, throwing an out of bounds exception if said index
		/// doesn't exist and a null reference exception if no internal reference is currently
		/// held.
		/// </summary>
		public T this[int i] => this.array[i];

		private ReadOnlySlice(T[] arr)
		{
			this.array = arr;
		}

		public IEnumerator<T> GetEnumerator()
		{
			return ((IEnumerable<T>)this.array).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.array.GetEnumerator();
		}

		public static implicit operator ReadOnlySlice<T>(Slice<T> slice) =>
				new ReadOnlySlice<T>(slice.Array);

		public static implicit operator ReadOnlySlice<T>(T[] array) => new ReadOnlySlice<T>(array);
	}

	/// <summary>
	/// Read-only reference to a linear block of memory of type <c>T</c>.
	///
	/// Unlike regular C# arrays, <see cref="Utility.ReadOnlySlice{T}" /> always contains a valid
	/// "Length" property that may be read from even when it is "<c>null</c>".
	/// </summary>
	public struct Slice<T> : IEnumerable<T>
	{
		private T[] array;

		/// <summary>
		/// Stored array.
		/// </summary>
		public T[] Array => this.array;

		/// <summary>
		/// Returns the number of elements in the internal reference.
		///
		/// A length of zero can indicate that the internal reference contains <c>0</c> items or
		/// that said reference is not currently set to a valid value.
		/// </summary>
		public int Length => ((this.array == null) ? 0 : this.array.Length);

		/// <summary>
		/// Reads and writes the element at <c>i<c/>, throwing an out of bounds exception if said
		/// index doesn't exist and a null reference exception if no internal reference is
		/// currently held.
		/// </summary>
		public ref T this[int i] => ref this.array[i];

		private Slice(T[] array)
		{
			this.array = array;
		}

		public IEnumerator<T> GetEnumerator()
		{
			return (IEnumerator<T>)this.array.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator<T>)this.array.GetEnumerator();
		}

		public static implicit operator Slice<T>(T[] array) => new Slice<T>(array);
	}
}
