﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.UI;
using UnityEngine.InputSystem.Users;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timeStamp;

    [SerializeField]
    private TextMeshProUGUI score;

    [SerializeField]
    private Image scoreIcon;

    [SerializeField]
    private Animator[] lives;

    [SerializeField]
    private Animation coinWobble;

    [SerializeField]
    private Animator clockAnimator;

    [SerializeField]
    private GameObject pauseMenu;

    [SerializeField]
    private GameObject firstPauseMenuObject;

    [SerializeField]
    private PlayerInput input;

    [SerializeField]
    private GameObject endScreen;

    [SerializeField]
    private GameObject failScreen;

    [SerializeField]
    private GameObject winScreen;

    [SerializeField]
    private TextMeshProUGUI finalScore;

    [SerializeField]
    private TextMeshProUGUI finalTime;

    [SerializeField]
    private AudioClip failTune = null;

    [SerializeField]
    private AudioClip winTune = null;

    private bool scoreSwelling;

    // Update is called once per frame
    void Start()
    {
        this.DeductLives();
        this.SetTime();
        this.score.text = GameManager.Instance.Score.ToString();

        GameManager.Instance.OnDeductLives.AddListener(this.DeductLives);
        GameManager.Instance.OnRestoreLives.AddListener(this.RestoreLives);
        GameManager.Instance.OnScoreChange.AddListener(this.SetScore);
        GameManager.Instance.OnTimerChange.AddListener(this.SetTime);
        GameManager.Instance.OnSuccess.AddListener(this.OnSuccess);
        GameManager.Instance.OnFailure.AddListener(this.OnFail);

        GameManager.Instance.OnPause.AddListener((isPlaying) => OnPause(!isPlaying));

        //InputSystem.onDeviceChange += OnControlsChange;
    }

    private void OnFail()
    {
        if (this.failTune != null)
        {
            GameManager.Instance.PlaySound(this.failTune);
        }

        this.failScreen.SetActive(true);
        this.winScreen.SetActive(false);

        this.SetFinalScoreAndTime();

        this.endScreen.SetActive(true);

    }

    private void OnSuccess()
    {
        if (this.winTune != null)
        {
            GameManager.Instance.PlaySound(this.winTune);
        }

        this.failScreen.SetActive(false);
        this.winScreen.SetActive(true);

        this.SetFinalScoreAndTime();

        this.endScreen.SetActive(true);
    }

    private void SetFinalScoreAndTime()
    {
        this.finalScore.text = this.score.text;

        int time = GameManager.Instance.Duration - GameManager.Instance.TimeLeftToPlay;
        int minutes = time / 60;
        int seconds = time % 60;
        if (seconds < 10)
        {
            this.finalTime.text = (minutes + ":0" + seconds);
        }
        else
        {
            this.finalTime.text = (minutes + ":" + seconds);
        }
    }

    private void OnPause(bool isPaused)
    {
        pauseMenu.SetActive(isPaused);
    }

    private void OnControlsChange(InputDevice inputDevice, InputDeviceChange change)
    {
        var eventSystem = EventSystem.current;
        if(inputDevice is Gamepad)
        {
            eventSystem.SetSelectedGameObject(null);
            var selectedObject = EventSystem.current.currentSelectedGameObject;
            if (selectedObject != null)
            {
                eventSystem.SetSelectedGameObject(selectedObject);
            }
        }
    }

    private void DeductLives()
    {
        for (int i = 0; i < lives.Length; i++)
        {
            var rectScale = this.lives[i].GetComponent<RectTransform>().lossyScale.x;
            if (i > GameManager.Instance.CurrentLives - 1 && rectScale > 0f)
            {
                this.lives[i].SetTrigger("DeductLife");
            }
        }
    }

    private void RestoreLives()
    {
        for (int i = 0; i < lives.Length; i++)
        {
            var rectScale = this.lives[i].GetComponent<RectTransform>().lossyScale.x;
            if (i <= GameManager.Instance.CurrentLives - 1 && rectScale == 0)
            {
                this.lives[i].SetTrigger("RestoreLife");
            }
        }
    }

    private void SetScore()
    {
        this.score.text = GameManager.Instance.Score.ToString();

        if(!this.scoreSwelling)
        {
            StartCoroutine(this.ScoreSwellRoutine());
        }
    }

    private void SetTime()
    {
        if(GameManager.Instance.TimeLeftToPlay < 11)
        {
            this.clockAnimator.SetBool("isEnding", true);
        }

        this.timeStamp.text = GameManager.Instance.Timestamp;
    }

    private IEnumerator ScoreSwellRoutine()
    {
        this.scoreSwelling = true;
        this.coinWobble.Play();
        while(this.coinWobble.isPlaying)
        {
            yield return null;
        }
        this.scoreSwelling = false;
    }
}
