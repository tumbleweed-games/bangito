﻿using Data;
using System.Collections;
using UnityEngine;
using Utility;

namespace Pouring
{
	public class Stream : MonoBehaviour
	{
		[SerializeField]
		private int ignoreLayer;

		[SerializeField]
		private float pourDistance = 2.0f;

		[SerializeField]
		private float pourSpeed = 3.0f;

		private LineRenderer lineRenderer = null;

		private ParticleSystem splashParticle = null;

		private Coroutine pouringCoroutine = null;

		private Vector3 targetPosition = new Vector3();

		private DrinkType drinkType;

		private void Awake()
		{
			if (!this.lineRenderer && !(this.lineRenderer = this.GetComponent<LineRenderer>()))
			{
				Debug.LogError("No LineRenderer found on " + this.name);
			}

			if (!this.splashParticle &&
			   !(this.splashParticle = this.GetComponentInChildren<ParticleSystem>()))
			{
				Debug.LogError("No Particle System in Children of " + this.name);
			}

			ignoreLayer = ~Layers.IgnorePouring.Mask;
			this.lineRenderer.enabled = false;
		}

		private void Start()
		{
			this.MoveToPositon(0, this.transform.position);
			this.MoveToPositon(1, this.transform.position);
		}

		/// <summary>
		/// Begins the coroutine to pour liquid
		/// </summary>
		public void Begin(DrinkType drinkType)
		{
			this.lineRenderer.enabled = true;
			this.drinkType = drinkType;

			this.lineRenderer.material.SetColor("_Tint", this.drinkType.liquidColor);
			StartCoroutine(this.UpdateParticle());
			this.pouringCoroutine = StartCoroutine(this.BeginPour());
		}

		/// <summary>
		/// Ends the coroutine to pour liquid
		/// </summary>
		public void End()
		{
			if (this.pouringCoroutine != null)
			{
				StopCoroutine(this.pouringCoroutine);
			}

			this.pouringCoroutine = StartCoroutine(EndPour());

			this.lineRenderer.enabled = false;
		}

		private System.Collections.IEnumerator BeginPour()
		{
			while (this.gameObject.activeSelf)
			{
				this.targetPosition = this.FindEndPoint();

				this.MoveToPositon(0, this.transform.position);
				this.AnimateToTargetPosition(1, this.targetPosition);

				if (this.lineRenderer.GetPosition(1) == this.targetPosition)
				{
					FillGlass();
				}

				yield return null;
			}
		}

		private IEnumerator EndPour()
		{
			while (!this.HasReachedPosition(0, this.targetPosition))
			{
				this.AnimateToTargetPosition(0, this.targetPosition);
				this.AnimateToTargetPosition(1, this.targetPosition);

				yield return null;
			}
		}

		private Vector3 FindEndPoint()
		{
			RaycastHit hit;
			Ray ray = new Ray(this.transform.position, Vector3.down);

			Physics.Raycast(ray, out hit, this.pourDistance, ignoreLayer);

			bool ret = (hit.collider && (hit.collider.gameObject != this.gameObject ||
										 hit.collider.gameObject != this.transform.parent));

			return ret ? hit.point : ray.GetPoint(pourDistance);
		}

		private void FillGlass()
		{
			RaycastHit hit;
			Ray ray = new Ray(this.transform.position, Vector3.down);

			if (Physics.Raycast(ray, out hit, this.pourDistance))
			{
				var hitGameObject = hit.collider.gameObject;

				if (hitGameObject != this.gameObject)
				{
					var drinkPickup =
							hitGameObject.GetComponentInParent<World.Pickup.DrinkPickup>();

					if (drinkPickup != null)
					{
						drinkPickup.AddDrink(this.drinkType);
					}
				}
			}
		}

		private void MoveToPositon(int index, Vector3 targetPos)
		{
			this.lineRenderer.SetPosition(index, targetPos);
		}

		private void AnimateToTargetPosition(int index, Vector3 targetPos)
		{
			Vector3 currentPoint = this.lineRenderer.GetPosition(index);
			Vector3 newPoint = Vector3.MoveTowards(
										currentPoint,
										targetPos,
										Time.deltaTime * pourSpeed);

			this.lineRenderer.SetPosition(index, newPoint);
		}

		private bool HasReachedPosition(int index, Vector3 targetPos)
		{
			return this.lineRenderer.GetPosition(index) == targetPos;
		}

		private IEnumerator UpdateParticle()
		{
			while (this.gameObject.activeSelf)
			{
				this.splashParticle.gameObject.transform.position = this.targetPosition;

				bool isHitting = this.HasReachedPosition(1, this.targetPosition);
				this.splashParticle.gameObject.SetActive(isHitting);

				yield return null;
			}
		}
	}
}
