using Data;
using Model;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Introduces a life-cycle to a <see cref="UnityEngine.GameObject" />.
/// </summary>
[RequireComponent(typeof(Rigidbody), typeof(AudioSource))]
public sealed class Actor : MonoBehaviour
{
	/// <summary>
	/// Callback triggered when damage is received.
	/// </summary>
	public sealed class DamageEvent : UnityEvent<int> {}

	[SerializeField]
	private Character character;

	[SerializeField]
	[HideInInspector]
	private CharacterModel model;

	private AudioSource audioSource;

	[SerializeField]
	private UnityEvent onDestroyed = new UnityEvent();

	private new Rigidbody rigidbody;

	private new ParticleSystem particleSystem;

	private int currentHealth;

	private float dialogPanelYOffset;

	/// <summary>
	/// Source of sounds made by the <see cref="Actor" />.
	/// </summary>
	public AudioSource AudioSource => this.audioSource;

	/// <summary>
	/// Base meta-information about an actor.
	/// </summary>
	public Character Character => this.character;

	/// <summary>
	/// Current actor health.
	/// </summary>
	public int Health => this.currentHealth;

	/// <summary>
	/// Visual worldspace representation.
	/// </summary>
	public CharacterModel Model => this.model;

	/// <summary>
	/// Event triggered upon any kind of game object destruction.
	/// </summary>
	public UnityEvent OnDestroyed => this.onDestroyed;

	public ParticleSystem ParticleSystem => this.particleSystem;

	/// <summary>
	/// The <see cref="UnityEngine.GameObject" />'s <see cref="UnityEngine.Rigidbody" /> component.
	/// </summary>
	public Rigidbody Rigidbody => this.rigidbody;

	public float DialogPanelYOffset => this.dialogPanelYOffset;

	/// <summary>
	/// Assigns a new character to the actor, replacing the current one.
	/// </summary>
	/// <param name="character">New character, or <c>null</c> to simply remove the current.</param>
	public void AssignCharacter(Character character)
	{
		BaseModel.DestroyModel(this.model);

		this.character = character;

		if (this.character == null)
		{
			this.model = null;
			// TODO: Should the character just be completely disabled if they're in this state?
			this.currentHealth = Character.defaultHealth;
		}
		else
		{
			this.model = BaseModel.
					InstantiateModel<CharacterModel>(this.character.modelPrefab, this.transform);

			if (this.model != null)
			{
				// Configure the model.
				var modelTransform = this.model.transform;
				modelTransform.position = this.transform.position;
				modelTransform.parent = this.transform;
			}

			this.currentHealth = this.character.maxHealth;
			this.dialogPanelYOffset = this.character.dialogPanelYOffset;
		}
	}

	private void Awake()
	{
		if (this.character == null)
		{
			// Fallback to a new, default character instance if one is not assigned.
			this.AssignCharacter(ScriptableObject.CreateInstance<Character>());
		}

		this.particleSystem = this.GetComponentInChildren<ParticleSystem>();
		this.audioSource = this.GetComponent<AudioSource>();
		this.rigidbody = this.GetComponent<Rigidbody>();
		this.rigidbody.freezeRotation = true;

		if (this.audioSource == null)
		{
			Debug.LogError("Failed to acquire UnityEngine.AudioSource");
		}

		this.currentHealth = this.character.maxHealth;
	}

	private void OnDestroy()
	{
		this.onDestroyed.Invoke();
	}
}

#if UNITY_EDITOR
/// <summary>
/// Custom Unity editor for <see cref="Actor" />s.
/// </summary>
[CustomEditor(typeof(Actor))]
public class ActorEditor : Editor
{
	private const string clearText = "Clear";

	private const string forceInvalidateText = "Force Invalidate Model";

	private Actor actorInstance = null;

	private Character characterSelected = null;

	private Character characterLast = null;

	/// <summary>
	/// Updates the Unity inspector GUI.
	/// </summary>
	public override void OnInspectorGUI()
	{
		EditorGUILayout.BeginHorizontal();
		{
			this.characterLast = this.characterSelected;

			this.characterSelected = (Character)EditorGUILayout.
					ObjectField("Character", this.characterSelected, typeof(Character), false);

			if (GUILayout.Button(clearText))
			{
				this.characterSelected = null;

				this.actorInstance.AssignCharacter(null);
				EditorUtility.SetDirty(this.actorInstance);
			}
		}
		EditorGUILayout.EndHorizontal();

		if ((this.characterSelected != this.characterLast) ||
				GUILayout.Button(forceInvalidateText))
		{
			// Update the assignment and force the serialized data to update.
			this.actorInstance.AssignCharacter(this.characterSelected);
			EditorUtility.SetDirty(this.actorInstance);
		}

		if (this.serializedObject != null)
		{
			EditorGUILayout.PropertyField(this.serializedObject.FindProperty("onDestroyed"));
			this.serializedObject.ApplyModifiedProperties();
		}
	}

	private void Awake()
	{
		this.actorInstance = (Actor)this.target;
		this.characterSelected = this.actorInstance.Character;
	}
}
#endif
