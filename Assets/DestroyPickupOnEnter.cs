﻿using UnityEngine;
using Utility;

[RequireComponent(typeof(Collider))]
public class DestroyPickupOnEnter : MonoBehaviour
{
    private Collider collision;

    private void Awake()
    {
        collision = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Pickup"))
        {
            Destroy(other.transform.parent.gameObject);
        }
    }
}
